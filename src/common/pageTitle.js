export function PageTitle() {
    const data = {
        originalTitle: '', // real title of this page before we add notification
    };

    return {
        setOriginalTitle: title => {
            data.originalTitle = title;
            document.title = title;
        },

        resetTitle: () => {
            document.title = data.originalTitle;
        },

        setNotificationTitle: nb => {
            document.title = '(' + nb + ') ' + data.originalTitle;
        },
    };
}