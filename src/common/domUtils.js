export function findAncestor(el, cls) {
    let currentElement = el.parentElement;
    while (currentElement && !currentElement.classList.contains(cls)) {
        currentElement = currentElement.parentElement;
    }
    return currentElement;
}

export function getCoords(elem) {
    const box = elem.getBoundingClientRect();
    const {
        body
    } = document;
    const docEl = document.documentElement;

    const scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    const scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

    const clientTop = docEl.clientTop || body.clientTop || 0;
    const clientLeft = docEl.clientLeft || body.clientLeft || 0;

    const top = box.top + scrollTop - clientTop;
    const left = box.left + scrollLeft - clientLeft;

    return {
        top: Math.round(top),
        left: Math.round(left)
    };
}

export function isTouchDevice() {
    return 'ontouchstart' in window || navigator.maxTouchPoints; // works on most browsers && works on IE10/11 and Surface
}

export function matchesPolyfill() {
    // From https://developer.mozilla.org/fr/docs/Web/API/Element/matches
    if (!Element.prototype.matches) {
        Element.prototype.matches =
            Element.prototype.matchesSelector ||
            Element.prototype.mozMatchesSelector ||
            Element.prototype.msMatchesSelector ||
            Element.prototype.oMatchesSelector ||
            Element.prototype.webkitMatchesSelector ||
            function(s) {
                var matches = (this.document || this.ownerDocument).querySelectorAll(s);
                var i = matches.length;
                while (--i >= 0 && matches.item(i) !== this) {}
                return i > -1;
            };
    }
}