export function numberWithSpaces(x) {
    if (typeof x !== 'number') return '';
    if (x === 0) return '0';
    if (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    }
    return '';
}

export default {
    numberWithSpaces
};