// @flow

export function dispatchRestrictedEvent(source: string) {
    window.dispatchEvent(
        new CustomEvent('open-restricted-modal', {
            detail: {
                source
            },
        }),
    );
}

export function protectFeature(source: string, feature: Function) {
    if (window.access_premium) {
        feature();
    } else {
        dispatchRestrictedEvent(source);
    }
}