import JSON_ from 'json_';

export function snakeToCamel(obj) {
    return JSON_.parse(JSON.stringify(obj));
}

export function camelToSnake(obj) {
    return JSON.parse(JSON_.stringify(obj));
}