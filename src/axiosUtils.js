import axios from 'axios';
import {
    camelToSnake,
    snakeToCamel
} from './caseUtils';

export function configAxios() {
    axios.interceptors.request.use(config => ({
        ...config,
        data: config.data ? camelToSnake(config.data) : config.data,
        params: config.params ? camelToSnake(config.params) : config.params,
    }));

    axios.interceptors.response.use(response => ({
        ...response,
        data: response.data ? snakeToCamel(response.data) : response.data,
    }));
}