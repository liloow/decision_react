export function getPosition(window, cursorPosition}) {
  const windowHeigth = window.innerHeight;
  const windowWidth = window.innerWidth;
  const tooltipYPosition = cursorPosition.y > windowHeigth / 2 ? 'top' : 'bottom';
  const tooltipXPosition = cursorPosition.x > windowWidth / 2 ? 'right' : 'left';
  return {
    tooltipYPosition,
    tooltipXPosition,
  }
}

export function getStyles(window, cursorPosition, tooltipYPosition, tooltipXPosition) {
  const pageHeight = Math.max(window.document.body.scrollHeight, window.document.body.offsetHeight);
  const arrowReversedClass = 'popover-pane-arrow--reversed';
  const verticalOffset = 5;
  const arrowOffset = 10;
  const arrowSize = 12;
  const scrollbarWidth = 30;
  const marginPopover = 20;
  const maxWidth = 400;
  const windowWidth = window.innerWidth;
  const scrollTop = window.pageYOffset;
  const tooltipStyle = {};
  const arrowStyle = {};
  let arrowClass = '';

  switch (tooltipYPosition) {
    case 'bottom': {
      const topOffset = cursorPosition.y + scrollTop + verticalOffset;
      tooltipStyle.bottom = null;
      tooltipStyle.top = `${topOffset + arrowOffset}px`;
      arrowStyle.bottom = 'auto';
      arrowStyle.top = `${topOffset}px`;
      break;
    }
    default: {
      arrowClass = arrowReversedClass;
      const bottomOffset = pageHeight - (cursorPosition.y + scrollTop) - verticalOffset;
      tooltipStyle.top = 'auto';
      tooltipStyle.bottom = `${bottomOffset + arrowOffset}px`;
      arrowStyle.top = 'auto';
      arrowStyle.bottom = `${bottomOffset}px`;
      break;
    }
  }

  switch (tooltipXPosition) {
    case 'right': {
      const availableSpace = cursorPosition.x - marginPopover;
      const unusedSpace = windowWidth - cursorPosition.x - marginPopover - scrollbarWidth;
      const missingSpace = Math.max(0, maxWidth - availableSpace);
      const offset = Math.min(missingSpace, unusedSpace);
      const rightOffset = tooltipYPosition === 'bottom' ? -5 : -11;

      tooltipStyle.left = 'auto';
      tooltipStyle.right = `${windowWidth - cursorPosition.x - scrollbarWidth - offset}px`;
      arrowStyle.left = 'auto';
      arrowStyle.right = `${windowWidth - cursorPosition.x - arrowSize + rightOffset}px`;

      break;
    }
    default: {
      const availableSpace = windowWidth - cursorPosition.x - scrollbarWidth;
      const unusedSpace = windowWidth - cursorPosition.x;
      const missingSpace = Math.max(0, maxWidth - availableSpace);
      const offset = Math.min(missingSpace, unusedSpace);
      const leftOffset = tooltipYPosition === 'bottom' ? 5 : 9;

      tooltipStyle.right = 'auto';
      tooltipStyle.left = `${cursorPosition.x - offset - scrollbarWidth}px`;
      arrowStyle.right = 'auto';
      arrowStyle.left = `${cursorPosition.x - arrowSize + leftOffset}px`;
      break;
    }
  }
  return {
    tooltipStyle,
    arrowStyle,
    arrowClass,
  };
}
