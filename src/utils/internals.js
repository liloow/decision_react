const XRegExp = require("xregexp");

var MONTHS = [
  "janvier",
  "février",
  "mars",
  "avril",
  "mai",
  "juin",
  "juillet",
  "août",
  "septembre",
  "octobre",
  "novembre",
  "décembre"
];

export function clean_debut(data, long_text) {
  data = data.trim();

  // Remove form feed characters
  data = data.replace(/\f/g, "");

  // Replace non breaking space
  data = data.replace(/\u00A0/g, " ");

  // Replace non breaking hyphen
  data = data.replace(/\u2011/g, "-");

  data = data.replace(/<font size="1">((.|[\s])*?)<\/font>/g, "$1");

  // Faux guillemets
  data = data.replace(/<< ?/g, "«&#8239;");
  data = data.replace(/ ?>>/g, "&#8239;»");
  // Normal br HTML
  data = data.replace(/<br [\w="]*>/g, "<br>");

  // Remove ECHR & ECJ links
  data = data.replace(/<a href="\.\..+?".*?>(.*?)<\/a>/g, "$1");
  data = data.replace(
    /<a href="www(.+?)".*?>(.*?)<\/a>/g,
    '<a href="$1" target="_blank" className="link">$2</a>'
  );

  // ADLC dead links
  // Ex: ADLC/1999/ADLC99-A-19
  data = data.replace(/<a href="-.html#?\d?">(.*?)<\/a>/g, "$1");

  // Remplace les doubles br par simple
  data = data.replace(/<br>\s<br>/g, "<br>");
  // Remplace les doubles br par simple (repasse pour les triplets)
  data = data.replace(/<br>\s<br>/g, "<br>");

  data = data.trim();

  // Fix opening/closing paragraph tag for contenu and resume
  if (long_text) {
    // Adding opening paragraph tag
    if (!/^<p.*?>/.test(data)) {
      data = "<p>" + data;
    }
    // Removing closing paragraph tag
    if (!/<\/p>$/.test(data)) {
      data += "</p>";
    }
  }

  // Remplace les br par des paragraphes
  data = data.replace(/((<br>)|(\n\n))/g, "</p><p>");

  // INPIO20115237 + DE201604081 examples \n
  data = XRegExp.replace(
    data,
    XRegExp("([^\\p{Ll}\\p{Z}])\n(\\p{Lu})"),
    "$1</p><p>$2",
    "all"
  );
  data = XRegExp.replace(
    data,
    XRegExp("([^\\p{L}])\n\\-"),
    "$1</p><p>-",
    "all"
  );
  // Other
  data = data.replace(/<p>\s*<\/p>/g, "");

  // Guillemet français ouvrant
  // Guillemet français fermant
  // Attention aux bugs potentiels avec class HTML
  // Vérification avec JURITEXT000031374097 CETATEXT000031938416 JURITEXT000032017784 JURITEXT000031861595
  // Mots encadrés
  data = XRegExp.replace(
    data,
    XRegExp('([^=])" ?([\\p{L}\\p{N} \\-,\\.\'\\/()°:%]+?) ?"'),
    "$1«&#8239;$2&#8239;»",
    "all"
  );
  // Début paragraphe
  data = data.replace(/<p>\s?"/g, "<p>«&#8239;");
  // Fin paragraphe
  data = data.replace(/ " ;/g, "&#8239»&#8239;;");

  // Remplace ... par points de suspension typographiques
  data = data.replace(/\.\.\./g, "…");
  // Recrée une espace après points de suspension lorsque bug anonymisation
  data = XRegExp.replace(data, XRegExp("…(\\p{L})"), "… $1", "all");
  // Remplace symboles par € quand erreur OCR
  data = data.replace(/(\d) ¿/g, "$1&nbsp;€");
  // Remplace apostrophe par apostrophe typographique
  // Bugs potentiels ?
  data = XRegExp.replace(data, XRegExp("(\\p{L})'([\\p{L} ])"), "$1’$2", "all");

  return data;
}

export function clean_fin(data) {
  // Espace insécable avant pourcentage
  data = data.replace(/(\d+) %/g, "$1&nbsp;%");
  // Dates propres
  data = XRegExp.replace(
    data,
    XRegExp("(\\d{1,2}) (\\p{L}{3,9}) (\\d{4})"),
    "$1&nbsp;$2&nbsp;$3",
    "all"
  );
  // Espace insécable après un nombre et avant le nom en rapport
  data = XRegExp.replace(
    data,
    XRegExp("(\\d+) (\\p{L}+)"),
    "$1&nbsp;$2",
    "all"
  );
  // Espace fine insécable pour séparer les milliers
  data = data.replace(/(\d+) (\d{3})/g, "$1&#8239;$2");
  // Espace insécable avant et après les heures
  data = data.replace(/heures (\d+)/g, "heures&nbsp;$1");
  // Non breaking space
  data = data.replace(/ :/g, "&nbsp;:");
  // Espace fine insécable avant signes de ponctuation double autres
  data = data.replace(/ (;|!|\?|»)/g, "&#8239;$1");
  data = data.replace(/« /g, "«&#8239;");
  // Espace insécable après L. R. :
  data = data.replace(/ L\. (\d+)/g, " L.&nbsp;$1");
  data = data.replace(/ R\. (\d+)/g, " R.&nbsp;$1");
  // Espace insécable (et exposant si nécessaire) après :
  // Monsieur
  // Messieurs
  data = XRegExp.replace(
    data,
    XRegExp("\\bM(M)?\\. ?(\\p{Lu})"),
    "M$1.&nbsp;$2",
    "all"
  );
  // Madame
  // Mesdames
  // Mademoiselle
  // Mesdemoiselles
  // Monseigneur
  data = XRegExp.replace(
    data,
    XRegExp("\\bM(me|lle|gr?)(s)? ?(\\p{Lu})"),
    "M<sup>$1$2</sup>&nbsp;$3",
    "all"
  );
  // Maître
  // Attention aux erreurs
  data = XRegExp.replace(
    data,
    XRegExp("\\bMe(s)? ?(\\p{Lu})"),
    "M<sup>e$1</sup>&nbsp;$2",
    "all"
  );
  // Maîtres
  // Attentions aux erreurs
  // Docteur
  data = XRegExp.replace(
    data,
    XRegExp("\\b(D|P)r(s)? ?(\\p{Lu})"),
    "$1<sup>r$2</sup>&nbsp;$3",
    "all"
  );
  // Docteurs
  // Professeur
  // Professeurs
  // Veuve
  data = XRegExp.replace(
    data,
    XRegExp("\\bVve ?(\\p{Lu})"),
    "V<sup>ve</sup>&nbsp;$1",
    "all"
  );
  // Compagnie
  data = XRegExp.replace(
    data,
    XRegExp("\\bCie ?(\\p{Lu})"),
    "C<sup>ie</sup>&nbsp;$1",
    "all"
  );
  // 1er
  data = data.replace(/\b1er(s)?\b/g, "1<sup>er$1</sup>");
  data = data.replace(/\b1[eéè]?re(s)?\b/g, "1<sup>re$1</sup>");
  data = data.replace(/\b(\d{1,2})i?[éeè](me)?\b/g, "$1<sup>e</sup>");
  // Après le décide
  // CETATEXT000007507598
  data = data.replace(/<p>{2,}/g, "<hr><p>");
  // Nobr n°
  data = data.replace(/n° /g, "n°&nbsp;");
  // Tiret cadratin début paragraphe
  data = data.replace(/<p>\s*- ?(?!\d)/g, "<p>—&#8239;");
  // Tiret demi-cadratin en incise
  data = data.replace(/ - /g, "&#8239;–&#8239;");

  return data;
}

export function french_month(month) {
  return MONTHS[month - 1];
}

export function french_day_html(day) {
  if (day === 1) return "1<sup>er</sup>";
  else return day.toString();
}
