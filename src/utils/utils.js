const {
    clean_debut,
    clean_fin,
    french_day_html,
    french_month
} = require('./internals');

export function clean_text(data) {
    if (data != null && typeof data === 'string' && data.length > 0) {
        data = clean_debut(data);
        data = clean_fin(data);
    }
    return data;
};

export function french_date_html(date) {
    if (date == null) return null;
    return french_day_html(date.getDate()) + ' ' + french_month(date.getMonth() + 1) + ' ' + date.getFullYear();
}

export function limitString(string, limit) {
    if (string.length > limit) {
        return string.slice(0, limit) + '...';
    } else {
        return string;
    }
}

export function citationJpExplanation(type) {
  const similar = 'décision proche';
  const confirm = 'solution similaire';
  const invalidate = 'solution contraire';
  const typeMessages = {
    RAPPR: similar,
    CONFER: similar,
    COMP: similar,
    SENS: confirm,
    CONFIRM: confirm,
    CONTR: invalidate,
    EVOLUTION: invalidate
  };
  if (!type) {
    return '(' + similar + ')';
  }
  if (type && typeMessages[type]) {
    return '(' + typeMessages[type] + ')';
  } else {
    return;
  }
};

export function capitalize(s) {
  if (!s) return s;
  return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
}