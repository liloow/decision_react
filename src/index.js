// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import { configAxios } from './axiosUtils';
import { ModalProvider } from './decision/modal/ModalContext';
import { ModalRoot } from './decision/modal/ModalRoot';
import { snakeToCamel } from './caseUtils';
import { ReportTimeSystem } from './report-time-system/ReportTimeSystem';
import { Root } from './decision/page/Root';

function renderId(id) {
  const root: ?HTMLElement = document.getElementById(id);
  if (root) {
    configAxios();
    setTimeout(() => {
      ReactDOM.render(
        <ReportTimeSystem entityType="decision" readKey={window.read_key}>
          <div className="decision__container" id="decision_container">
            <ModalProvider>
              <Root
                arretInfos={snakeToCamel(window.arret)}
                accessPremium={window.access_premium}
                decisionComments={snakeToCamel(window.decision_comments)}
              />,
              <ModalRoot />
            </ModalProvider>
          </div>
        </ReportTimeSystem>,
        root,
      );
    }, 500);
  }
}

renderId('root');
