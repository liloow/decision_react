// @flow

import axios from 'axios';

// const cancel = axios.CancelToken.source();
// const api = axios.create({
//   cancekToken: cancel.token,
// });

export function fetchSimilarLawDecisions(doc_id: string, key: string) {
  return axios.get('https://cors.io/?https://www.doctrine.fr/getSimilarLawDecisions?', {
    params: {
      doc_id,
      key,
    },
  });
}

export function fetchSimilarDecisions(doc_id: string, key: string) {
  return axios.get('https://cors.io/?https://www.doctrine.fr/getSimilarDecisions?', {
    params: {
      doc_id,
      key,
    },
  });
}

export function fetchReferenceDecisions(doc_id: string, key: string) {
  return axios.get('https://cors.io/?https://www.doctrine.fr/getReferenceDecisions?', {
    params: {
      doc_id,
      key,
    },
  });
}

export function fetchFicheInfo(keyword: string) {
  return axios.get('https://cors.io/?https://www.doctrine.fr/ajax_get_keyword?', {
    params: {
      keyword,
    },
  });
}

export function fetchLawInfo(doc_id: string) {
  return axios.get('https://cors.io/?https://www.doctrine.fr/ajax_get_law?', {
    params: {
      doc_id,
    },
  });
}

// export function moreLikeThis(selectedText: string, docId: string, docKey: string) {
//   console.log(arguments)
//   return axios.post('https://cors.io/?https://www.doctrine.fr/more_like_this', {
//     responseType: 'json',
//     data: {
//       selectedText,
//       docId,
//       docKey,
//       },
//       mode: 'no-cors',
//     headers: {
//       'Access-Control-Allow-Origin': '*',
//       Accept: 'application/json',
//       'Content-Type': 'application/json',
//     },
//     }
//   )
// }

export function moreLikeThis(selectedText: string, docId: string, docKey: string) {
  return fetch('https://www.doctrine.fr/more_like_this', {
    body: JSON.stringify({
      like: 'Attendu que les époux Saint-Arroman ont assigné le Direc­teur de la Réunion des Musées nationaux, Rheims, ancien commissaire-priseur, Lebel, expert, et Laurin, commissaire-priseur, pour : 1) entendre prononcer la nul­lité de la vente aux enchères publiques, faite au prix de 2 200 F et à la date du 21 février 1968, d’un tableau offert comme une œuvre de l’école des Carrache et dont l’auteur serait en réalité Nicolas Poussin; 2) subsidiairement s’entendre Rheims et Lebel condamner à payer aux demandeurs une indemnité provisionnelle de 150 000 F et entendre ordonner une expertise en vue d’évaluer le préjudice subi par ceux-ci;\n\n',
      doc_id: 'DE8759599495785446',
      key: '0b15471f593e016e8780da4359538bc9',
    }),
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'no-cors', // no-cors, cors, *same-origin
    headers: {
      'content-type': 'application/json',
      Accept: 'application/json, text/plain, */*',
    },
  });
}