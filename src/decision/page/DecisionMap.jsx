// @flow

import React, { Component } from "react";
import { drawPath, sync } from "./decoratePageMap";

type Props = {|
  decisionMap: Array<{
    id_tag: string,
    title: string
  }>,
  decisionMapData: {
    has_summary: boolean,
    has_chrono: boolean
  },
  accessPremium: boolean
|};

type State = {
  anchors: Array<Array<string>>,
  style: {
    position: string
  },
  height: number,
  dmap: ?HTMLElement,
  dmapPath: ?HTMLElement,
  dmapItems: Array<{}>
};

export class DecisionMap extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      anchors: [
        ["decision-title", "Titre"],
        props.decisionMapData.has_summary
          ? ["decision_title-summary", "Résumé"]
          : [],
        props.decisionMapData.has_chrono
          ? ["decision_title-chrono", " Chronologie de l’affaire"]
          : [],
        ["decision_title-infos", "Informations"],
        ["decision_title-text", "Texte intégral"]
      ].filter(el => el.length > 0),
      style: {
        width: "240px",
        display: "block",
        height: "auto",
        position: "fixed",
        top: "90px",
        bottom: "auto",
        paddingBottom: "0px"
      },
      height: 0,
      dmap: null,
      dmapPath: null,
      dmapItems: []
    };
  }

  scrollToAnchor = (id: string) => {
    let anchor = document.getElementById(id);
    if (anchor != null) anchor.scrollIntoView({ behavior: "smooth" });
  };

  fixMap = () => {
    let mainContainer = document.getElementById("DecisionLayout");
    if (mainContainer) {
      let bottom = mainContainer.offsetHeight - 30;
      if (
        bottom - window.scrollY - this.state.height < 0 &&
        this.state.style.position !== "absolute"
      ) {
        this.setState({
          style: {
            ...this.state.style,
            position: "absolute",
            top: bottom - 90
          }
        });
      }
      if (
        bottom - window.scrollY - this.state.height > 0 &&
        this.state.style.position !== "fixed"
      ) {
        this.setState({
          style: {
            ...this.state.style,
            position: "fixed",
            top: "90px"
          }
        });
      }
    }
  };

  sync = sync;
  drawPath = drawPath;

  async componentDidMount() {
    const ancestor = document.getElementById("map_container");
    const dmap = document.getElementById("decision__map");
    const dmapPath = document.querySelector(".dmap-marker path");
    const dmapItems = await drawPath(dmap, dmapPath);
    this.setState({
      dmap: dmap,
      dmapPath: dmapPath,
      dmapItems: dmapItems,
      height: ancestor ? ancestor.offsetHeight : 0
    });
    window.addEventListener(
      "resize",
      this.drawPath(this.state.dmap, this.state.dmapPath),
      false
    );
    window.addEventListener(
      "scroll",
      () =>
        this.sync(this.state.dmap, this.state.dmapPath, this.state.dmapItems),
      false
    );
    window.addEventListener("scroll", this.fixMap, false);
  }

  render() {
    return (
      <div className="show_on--desktop--large">
        <div className="decision__map__empty"> </div>
        <div
          id="map_container"
          className="navigation_map"
          style={this.state.style}
        >
          <div className="decision__map__title"> Plan de la décision </div>{" "}
          <div
            className="decision__map dmap"
            id="decision__map"
            style={{ maxHeight: "none", height: "100%", overflowY: "hidden" }}
          >
            {this.state.anchors.map(anchor => (
              <a
                className="decision__map__subtitle__generic"
                key={anchor[0]}
                target={anchor[0]}
                onClick={() => this.scrollToAnchor(anchor[0])}
              >
                <span className="decision__map__subtitle__generic__label">
                  {anchor[1]}
                </span>
              </a>
            ))}
            <ul>
              {this.props.decisionMap.length > 0 &&
                this.props.decisionMap.map(item => (
                  <li className="decision__map__subtitle">
                    <a classMame="decision__map__subtitle" target={item.id_tag}>
                      {item.title}
                    </a>
                  </li>
                ))}
            </ul>
            <svg
              className="dmap-marker"
              width="200"
              height="200"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                stroke="#444"
                strokeWidth="3"
                fill="transparent"
                strokeDasharray="0, 0, 0, 1000"
                strokeLinecap="round"
                strokeLinejoin="round"
                transform="translate(-0.5, -0.5)"
              />
            </svg>
          </div>
        </div>
      </div>
    );
  }
}
