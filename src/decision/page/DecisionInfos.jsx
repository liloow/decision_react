// @flow

import React from "react";
import { HeaderReference } from "./header/HeaderReference";
import { HeaderJurisdiction } from "./header/HeaderJurisdiction";
import { HeaderAux } from "./header/HeaderAux";
import { HeaderCedh } from "./header/HeaderCedh";
import { HeaderDates } from "./header/HeaderDates";
import { HeaderInpi } from "./header/HeaderInpi";
import { HeaderLinks } from "./header/HeaderLinks";
import { HeaderNumber } from "./header/HeaderNumber";
import { HeaderParties } from "./header/HeaderParties";
import { HeaderTitle } from "./header/HeaderTitle";
import { HeaderPubli } from "./header/HeaderPubli";
import { HeaderType } from "./header/HeaderType";
import { HeaderSolution } from "./header/HeaderSolution";
import { HeaderSource } from "./header/HeaderSource";
import type { arrets } from "../types";

type Props = {|
    arret: arrets
|};

export function DecisionInfos(props: Props) {
  return (
    <div className="decision__header">
      <div className="decision__header__meta">
        <HeaderReference arret={props.arret} />
        <HeaderJurisdiction arret={props.arret} />
        <HeaderNumber arret={props.arret} />
        <HeaderTitle arret={props.arret} />
        <HeaderPubli arret={props.arret} />
        <HeaderParties arret={props.arret} />
        <HeaderType arret={props.arret} />
        <HeaderDates arret={props.arret} />
        <HeaderAux arret={props.arret} />
        <HeaderLinks arret={props.arret} />
        <HeaderCedh arret={props.arret} />
        <HeaderInpi arret={props.arret} />
        <HeaderSolution arret={props.arret} />
        <HeaderSource arret={props.arret} />
      </div>
    </div>
  );
}
