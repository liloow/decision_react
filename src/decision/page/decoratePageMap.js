// Skip flow beacause of SVGPath

const TOP_MARGIN = 0.2;
const BOTTOM_MARGIN = 0.2;

export function drawPath(dmap, dmapPath) {
  let path = [];
  let pathIndent;
  let dmapItems = [].slice.call(dmap.querySelectorAll("a")).map(el => ({
    listItem: el,
    anchor: el,
    target: document.getElementById(el.getAttribute("target")).parentElement
  }));
  dmapItems.forEach((el, i) => {
    let x = el.anchor.offsetLeft - 5;
    let y = el.anchor.offsetTop;
    let height = el.anchor.offsetHeight;

    if (i === 0) {
      el.pathStart = 0;
      path.push("M", x, y, "L", x, y + height);
    } else {
      if (pathIndent !== x) path.push("L", pathIndent, y);

      path.push("L", x, y);
      dmapPath.setAttribute("d", path.join(" "));
      el.pathStart = dmapPath.getTotalLength() || 0;
      path.push("L", x, y + height);
    }
    pathIndent = x;
    dmapPath.setAttribute("d", path.join(" "));
    el.pathEnd = dmapPath.getTotalLength();
  });
  sync(dmap, dmapPath, dmapItems);
  return dmapItems;
}

export function sync(dmap, dmapPath, dmapItems) {
  let windowHeight = window.innerHeight;
  let pathLength = dmapPath.getTotalLength();
  let pathStart = pathLength;
  let pathEnd = 0;
  let visibleItems = 0;

  dmapItems.forEach((el, i, a) => {
    let targetBounds = el.target.getBoundingClientRect();
    if (
      targetBounds.bottom > windowHeight * TOP_MARGIN &&
      targetBounds.top < windowHeight * (1 - BOTTOM_MARGIN)
    ) {
      pathStart = Math.min(el.pathStart, pathStart);
      pathEnd = Math.max(el.pathEnd, pathEnd);
      visibleItems += 1;
      el.listItem.classList.add("visible");
    } else {
      el.listItem.classList.remove("visible");
    }
  });

  if (visibleItems > 0 && pathStart < pathEnd) {
    dmapPath.setAttribute("stroke-dashoffset", "1");
    dmapPath.setAttribute(
      "stroke-dasharray",
      `1, ${pathStart}, ${pathEnd - pathStart}, ${pathLength}`
    );
    dmapPath.setAttribute("opacity", 1);
  } else {
    dmapPath.setAttribute("opacity", 0);
  }
}
