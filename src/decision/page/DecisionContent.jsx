// @flow

import React from "react";
import { DecisionContentBlock } from "./DecisionContentBlock";
import { DecisionForSelect } from "./DecisionForSelect";
import { CommentsArticles } from "./aside/CommentsArticles";
import type { comments, arrets } from "../types";
type Props = {|
  docId: string,
  document_read_key: string,
  contenuHtml: string,
  accessPremium: boolean,
  arret: arrets,
  decisionComments: Array<comments>,
  decisionMapData: {
    has_summary: boolean,
    has_chrono: boolean
  }
|};

export function DecisionContent(props: Props) {
  const linksDecisions = window.datasetarray
  const { decisionComments, decisionMapData, document_read_key, docId, arret, accessPremium, contenuHtml, ...prop } = props;
  return (
    <div className="decision__content">
      <CommentsArticles
        showOnTablet={true}
        decisionComments={decisionComments}
      />
      <DecisionContentBlock
        decisionComments={decisionComments}
        accessPremium={accessPremium}
        decisionMapData={decisionMapData}
        document_read_key={document_read_key}
        docId={docId}
        arret={arret}
        contenuHtml={contenuHtml}
        linksDecisions={linksDecisions}
      />
      <DecisionForSelect accessPremium={props.accessPremium} document_read_key={props.document_read_key} docId={docId} contenuHtml={contenuHtml} arret={props.arret} />
    </div>
  );
}
