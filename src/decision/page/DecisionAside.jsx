// @flow

import React from "react";
import { CommentsArticles } from "./aside/CommentsArticles";
import { ReferenceDecisions } from "./aside/ReferenceDecisions";
import { SimilarDecisions } from "./aside/SimilarDecisions";
import { DidYouKnow } from "./aside/DidYouKnow";
import type { comments } from "../types";

type Props = {|
  docId: string,
  document_read_key: string,
  decisionComments: Array<comments>
|};

export function DecisionAside(props: Props) {
  return (
    <div className="decision__aside hide_on--print" id="decision__aside">
      <CommentsArticles
        showOnTablet={false}
        docId={props.docId}
        decisionComments={props.decisionComments}
      />
      <ReferenceDecisions
        docId={props.docId}
        document_read_key={props.document_read_key}
      />
      <SimilarDecisions
        docId={props.docId}
        document_read_key={props.document_read_key}
      />
      <DidYouKnow />
    </div>
  );
}
