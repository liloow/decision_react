import React, { Component } from 'react';

export class HeaderType extends Component {
  render() {
    return (
      <% if (present(arret.type_rec)) { %>
        <div className="decision__header__meta__item">
            <span className="decision__strong">Type de recours :</span> <%= capitalize(arret.type_rec) %>
        </div>
      <% } %>

      <!-- Type de décision pour la CEDH avec traduction en français -->
      <% if (present(arret.type_decision_cedh)) { %>
        <div className="decision__header__meta__item">
          <span className="decision__strong">Type de document :</span> <%= arret.type_decision_cedh %>
        </div>
      <% } %>

      <!-- Type de décision pour le Conseil constitutionnel -->
      <% if (present(arret.type_decision_cc)) { %>
        <div className="decision__header__meta__item">
          <span className="decision__strong">Type de décision :</span> <%- arret.type_decision_cc %>
        </div>
      <% } %>
    );
  }
}


