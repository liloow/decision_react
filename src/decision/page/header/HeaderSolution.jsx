import React, { Component } from 'react';
import { capitalize } from '../../../utils/utils'

export class HeaderSolution extends Component {

  state = {
    data: [],
  }  

  componentDidMount() {
    const data = []
    if (window.arret.solutions != null) { data.push({
        key: 'Solution: ',
        value: window.arret.solutions.forEach(solution => solution)
      })
    }
    if (window.arret.etat_juridique != null) { data.push({
        key: 'État: ',
        value: window.arret.etat_juridique
      })
    }
    if (window.arret.solution != null && window.arret.juridiction === 'CEDH' ) { data.push({
        key: `Conclusion${/;/.test(window.arret.solution) ? 's' : ''}: `,
        value: window.arret.solution.replace(/;/g,'&nbsp;; ')
      })
    }
    if (window.arret.solution != null && window.arret.juridiction !== 'CEDH') { data.push({
        key:  window.arret.juridiction === 'CNIL' ? 'Nature de la délibération: ' : 'Dispositif',
        value: capitalize(window.arret.solution)
      })
    }
    this.setState({
      data: data
    })
  }

  render() {
    return (
        (this.state.data.map(el => (
        <div className="decision__header__meta__item">
          <span className="decision__strong">{el.key}</span> 
          {el.value}
        </div>)
      ))
    )
  }
}
