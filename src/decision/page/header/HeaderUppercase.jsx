import React, { Component } from 'react';

export class HeaderTitle extends Component {
  render() {
    return (
      <!-- Titre pour les DC du CC -->
      <% if (present(arret.titre) && ((arret.juridiction == 'CONSTIT' && arret.nature_abrev_cc != 'DC') || ((arret.field_origin.titre == 'INPI') && (arret.juridiction != 'INPI')) || arret.juridiction == 'CCOMPTES' )) { %>
        <div className="decision__header__meta__item">
          <span className="decision__strong"><%= arret.titre %></span>
        </div>
      <% } %>

      <!-- Titre original CJUE -->
      <% if (present(arret.titre_original)) { %>
        <div className="decision__header__meta__item">
          <span className="decision__strong"><%= arret.titre_original %></span>
        </div>
      <% } %>


      <!-- Loi déférée devant le Conseil constitutionnel -->
      <% if (present(arret.loi_def)) { %>
        <div className="decision__header__meta__item">
          <span className="decision__strong">Loi déférée :</span>
          <% if (present(arret.loi_def_nor) && arret.loi_def_nor != 'SUPPRIME') { %>
            <a href="http://www.legifrance.gouv.fr/WAspad/UnTexteDeJorf?numjo=<%= arret.loi_def_nor.replace(/\./g,'') %>" target="_blank"><%= arret.loi_def %></a>
          <% } else {%>
            <%= arret.loi_def %>
          <% } %>
        </div>
      <% } %>
    );
  }
}


