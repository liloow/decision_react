// @flow

import React, { Component } from "react";
import { clean_text } from "../../../utils/utils";
import type { arrets } from '../../types'


function JudgeType(props: {juridiction: string, president: string}) {
  const s = props.president.includes(';') ? "s" : "";
  if (props.juridiction === "CEDH") return <span className="decision__strong">Juge{s}: </span>;
  if (props.juridiction === "CJUE") return <span className="decision__strong">Juge-rapporteur{s}:</span>;
  return <span className="decision__strong">Président: </span>;
}

export class HeaderAux extends Component<arrets> {

  render() {
    return (
      this.props && (
        <div>
          {this.props.president && this.props.juridiction && (
            <div className="decision__header__meta__item">
              <JudgeType juridiction={this.props.juridiction} president={this.props.president} />
              <span>
                {clean_text(this.props.president.replace(/;/g, ", "))}
              </span>
            </div>
          )}
          {this.props.rapporteur && (
            <div className="decision__header__meta__item">
              <span className="decision__strong">Rapporteur :</span>
              {clean_text(this.props.rapporteur)}
            </div>
          )}
          {this.props.avocat_gl && (
            <div className="decision__header__meta__item">
              <span className="decision__strong">Avocat général :</span>
              {clean_text(this.props.avocat_gl)}
            </div>
          )}
          {this.props.commissaire_gvt && (
            <div className="decision__header__meta__item">
              <span className="decision__strong">Rapporteur public :</span>
              {clean_text(this.props.commissaire_gvt)}
            </div>
          )}
          {this.props.rapporteurs && (
            <div className="decision__header__meta__item">
              <span className="decision__strong">Rapporteur(s) :</span>
              {this.props.rapporteurs}
            </div>
          )}
          {this.props.reviseurs && (
            <div className="decision__header__meta__item">
              <span className="decision__strong">Réviseur(s) :</span>
              {this.props.reviseurs}
            </div>
          )}
          {this.props.avocats && (
            <div className="decision__header__meta__item">
              <span className="decision__strong">
                Avocat{this.props.avocats.includes(";") ? "s" : ""}:{" "}
              </span>
              {clean_text(this.props.avocats)}
            </div>
          )}
        </div>
      )
    );
  }
}
