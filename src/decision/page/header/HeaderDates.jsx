import React, { Component } from 'react';
import { french_date_html } from '../../../utils/utils'

export class HeaderDates extends Component {

  state = {
    data: [],
  }  

  componentDidMount() {
    const data = []
    if (window.arret.introduction_date != null) { data.push({
        key: 'Date d\'introduction: ',
        value: french_date_html(window.arret.introduction_date)
      })
    }
    if (window.arret.judgement_date != null) { data.push({
        key: 'Date de jugement: ',
        value: french_date_html(window.arret.judgement_date)
      })
    }
    if (window.arret.cjue_date_request_opinion != null) { data.push({
        key: 'Date de dépôt: ',
        value: french_date_html(window.arret.cjue_date_request_opinion)
      })
    }
    if (window.arret.dates_seance != null) { data.push({
        key: 'Date de jugement: ',
        value: french_date_html(window.arret.dates_seance)
      })
    }
        if (window.arret.date_lecture != null) { data.push({
        key: 'Date de lecture: ',
        value: french_date_html(window.arret.date_lecture)
      })
    }
    if (window.arret.date_doc != null) { data.push({
        key: 'Date du document: ',
        value: french_date_html(window.arret.date_doc)
      })
    }
    this.setState({
      data: data
    })
  }

  render() {
    return (
        (this.state.data.map((el, i) => (
        <div key={i} className="decision__header__meta__item">
          <span className="decision__strong">
            {el.key}
          </span> 
          <span dangerouslySetInnerHTML={{__html: el.key}}></span>
        </div>)
      ))
    )
  }
}
