// @flow

import React, { Component } from "react";
import type { arrets } from '../../types'

export function HeaderCedh(props: arrets) {
  const data: Array<{key: string, value: string}> = [];
  const importance = {
    '1': "Publiée au Recueil",
    '2': "Importance élevée",
    '3': "Importance moyenne",
    '4': "Importance faible"
  }
  if (props.importance != null) {
    data.push({
      key: "Niveau d'importance: ",
      value: this.importance[String(props.importance)]
    });
  }
  if (props.separateOpinion != null) {
    data.push({
      key: "Opinion(s) séparée(s): ",
      value: props.separateOpinion ? "Oui" : "Non"
    });
  }

  return  data.map( (el, i) => (
    <div className="decision__header__meta__item" key={i}>
      <span className="decision__strong">{el.key}</span>
      {el.value}
    </div>
  ))
}
