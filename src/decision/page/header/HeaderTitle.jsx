import React, { Component } from 'react';

export class HeaderTitle extends Component {

  state = {
    data: [],
  }  

  componentDidMount() {
    const data = []
    if ((window.arret.titre != null) 
      && ((window.arret.juridiction === 'CONSTIT' && window.arret.nature_abrev_cc !== 'DC') 
        || ((window.arret.field_origin.titre === 'INPI') && (window.arret.juridiction !== 'INPI')) 
        || window.arret.juridiction === 'CCOMPTES' )) { data.push({
        key: '',
        value: window.arret.titre,
      })
    }
    if (window.arret.titre_original != null) { data.push({
        key: '',
        value: window.arret.titre_original,
      })
    }
    if (window.arret.loi_def != null) { data.push({
        key: 'Loi déférée: ',
        value: (window.arret.loi_def_nor != null && window.arret.loi_def_nor !== 'SUPPRIME')
               ? (<a href={`http://www.legifrance.gouv.fr/WAspad/UnTexteDeJorf?numjo=${window.arret.loi_def_nor.replace(/\./g,'')}`} target="_blank">{window.arret.loi_def}</a>)
               : window.arret.loi_def
      })
    }
    this.setState({
      data: data
    })
  }

  render() {
    return (
        (this.state.data.map(el => (
        <div className="decision__header__meta__item">
          <span className="decision__strong">{el.key}</span> 
          {el.value}
        </div>)
      ))
    )
  }
}

