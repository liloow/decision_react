import React from "react";
import { french_date_html } from "../../../utils/utils";

export function HeaderSource(props) {
  return (
    props.cdc_ref != null &&
    <div className="decision__header__meta__item">
      <span className="decision__strong">Identifiant Cour des comptes :</span>
      {props.cdc_ref}
    </div>(props.cjue_ref != null) &&
    <div className="decision__header__meta__item">
      <span className="decision__strong">Identifiant CELEX :</span>
      {window.access_premium && (
        <a
          className="decision__sourcelink decision__header__item__link"
          href={`http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=CELEX${
            props.cjue_ref
          }`}
        >
          {props.cjue_ref}
        </a>
      )}
      {!window.access_premium && (
        <a
          className="decision__sourcelink decision__header__item__link"
          ng-click="decisionVm.linkSourceClicked('link', 'CJUE')"
        >
          {props.cjue_ref}
        </a>
      )}
    </div>(props.inpi_ref != null) &&
    <div className="decision__header__meta__item">
      <span className="decision__strong">Référence INPI :</span>
      {props.inpi_ref}
    </div>(props.legifrance_ref != null) &&
    <div className="decision__header__meta__item">
      <span className="decision__strong">Identifiant Légifrance :</span>
      {window.access_premium && (
        <a
          className="decision__sourcelink decision__header__item__link"
          href={window.legifrance_link}
        >
          {props.legifrance_ref}
        </a>
      )}
      {!window.access_premium && (
        <a
          className="decision__sourcelink decision__header__item__link"
          ng-click="decisionVm.linkSourceClicked('link', 'Legifrance')"
        >
          {props.legifrance_ref}
        </a>
      )}
    </div>(props.adlc_ref != null) &&
    <div className="decision__header__meta__item">
      <span className="decision__strong">Identifiant ADLC :</span>
      {window.access_premium && (
        <a
          className="decision__sourcelink decision__header__item__link"
          href="http://www.autoritedelaconcurrence.fr/user/avisdec.php?numero= {props.adlc_ref}"
        >
          {" "}
          {props.adlc_ref}
        </a>
      )}
      {!window.access_premium && (
        <a
          className="decision__sourcelink decision__header__item__link"
          ng-click="decisionVm.linkSourceClicked('link', 'ADLC')"
        >
          }
          {props.adlc_ref}
        </a>
      )}
    </div>(props.juridiction === "AMF") &&
    <div className="decision__header__meta__item">
      <span className="decision__strong">Identifiant AMF :</span>
      {props.numero}
    </div>(props.cedh_ref != null) &&
    <div className="decision__header__meta__item">
      <span className="decision__strong">Référence HUDOC :</span>
      {window.access_premium && (
        <a
          className="decision__sourcelink decision__header__item__link"
          href="http://hudoc.echr.coe.int/fre?i= {props.cedh_ref}"
        >
          {props.cedh_ref}
        </a>
      )}
      {!window.access_premium && (
        <a
          className="decision__sourcelink decision__header__item__link"
          ng-click="decisionVm.linkSourceClicked('link', 'HUDOC')"
        >
          }
          {props.cedh_ref}
        </a>
      )}
    </div>(props.ecli != null) &&
    <div className="decision__header__meta__item">
      <span className="decision__strong">Identifiant européen : </span>{" "}
      {props.ecli}
    </div>(props.url_original) &&
    <div className="decision__header__meta__item hide_on--print">
      {window.access_premium && (
        <a className="decision__sourcelink" href={props.url_original}>
          Lire la décision sur le site de la juridiction
        </a>
      )}
      {!window.access_premium && (
        <a
          className="decision__sourcelink"
          ng-click="decisionVm.linkSourceClicked('link', 'url_original')"
        >
          Lire la décision sur le site de la juridiction
        </a>
      )}
    </div>(props.nor != null && false)(
      <div className="decision__header__meta__item">
        <span className="decision__strong">NOR : </span> {props.nor}
      </div>
    )(props.cjue_date_jo != null) &&
    <div className="decision__header__meta__item">
      <span className="decision__strong">Journal officiel :</span>
      <span className="single__header__item__text">
        JO
        {props.cjue_sous_serie_jo != null && props.cjue_sous_serie_jo}
        {props.cjue_num_jo != null && props.cjue_num_jo} du{" "}
        {french_date_html(props.cjue_date_jo)}
      </span>
    </div>(window.original_pdf) && (
      <div className="decision__header__source">
        <div className="decision__header__meta__item">
          {window.access_premium && (
            <a
              className="decision__strong decision__sourcelink decision__header__item__link"
              href={window.original_pdf}
            >
              {window.constants.WORDING_ORIGINAL_PDF}
            </a>
          )}
          {!window.access_premium && (
            <a
              className="decision__strong decision__sourcelink decision__header__item__link"
              ng-click="decisionVm.linkSourceClicked('PDF', 'original_pdf')"
            >
              {window.constants.WORDING_ORIGINAL_PDF}
            </a>
          )}
        </div>)
      </div>
    )
  );
}
