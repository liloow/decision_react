import React, { Component } from 'react';

export class HeaderJurisdiction extends Component {
  render() {
    return (
      <div className="decision__header__meta__item">
        <span className="decision__strong">Juridiction :</span> <a href="/c/JUR8A6E87E145F4B88DE1B5?source=decisionPageLink" className="link fichejurisdictionlink" target="_self">{window.arret.juridiction_francais_avec_ville}</a>
      </div>
    );
  }
}


