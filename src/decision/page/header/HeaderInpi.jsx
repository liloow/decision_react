import React, { Component } from 'react';
import { clean_text } from '../../../utils/utils'

export class HeaderInpi extends Component {

  state = {
    data: [],
  }  

  componentDidMount() {
    const data = []
    if (window.arret.domaine_pi != null) { data.push({
        key: 'Domaine propriété intellectuelle: ',
        value: window.arret.domaine_pi
      })
    }
    if (window.arret.inpi_marq != null) { data.push({
        key: 'Marques: ',
        value: clean_text(window.arret.inpi_marq)
      })
    }
    if (window.arret.inpi_obj != null) { data.push({
        key: 'Numéro(s) d\'enregistrement des titres de propriété industrielle: ',
        value: (window.arret.inpi_obj)
      })
    }
    if (window.arret.inpi_ti != null) { data.push({
        key: 'Titre du brevet: ',
        value: (window.arret.inpi_ti)
      })
    }
        if (window.arret.inpi_cib != null) { data.push({
        key: 'Classification internationale des brevets: ',
        value: (window.arret.inpi_cib)
      })
    }
    if (window.arret.inpi_ct != null) { data.push({
        key: 'Brevets cités autres que les brevets mis en cause: ',
        value: (window.arret.inpi_ct)
      })
    }
   if (window.arret.inpi_cl != null) { data.push({
        key: 'Classification internationale des marques: ',
        value: (window.arret.inpi_cl)
      })
    }
    if (window.arret.inpi_cld != null) { data.push({
        key: 'Classification internationale des dessins et modèles: ',
        value: (window.arret.inpi_cld)
      })
    }
    if (window.arret.inpi_prod != null) { data.push({
        key: 'Liste des produits ou services désignés: ',
        value: (window.arret.inpi_prod)
      })
    }
    this.setState({
      data: data
    })
  }

  render() {
    return (
        (this.state.data.map( (el, i) => (
        <div key={i} className="decision__header__meta__item">
          <span className="decision__strong">{el.key}</span> 
          {el.value}
        </div>)
      ))
    )
  }
}
