import React, { Component } from 'react';

export class HeaderNumber extends Component {

  state = {
    data: [],
  }  

  componentDidMount() {
    const data = []
    if (window.arret.numero_affaire != null) { data.push({
        key: `Numéro(s) ${window.arret.juridiction === 'CASS' ? 'de pourvoi: ' : ': ' }`,
        value: window.arret.juridiction === 'CEDH' ? window.arret.numero_affaire.replace(/;/g, ', ') : window.arret.numero_affaire
      })
    }
    if (window.arret.numero != null && window.arret.numero_affaire == null) { data.push({
        key: window.arret.juridiction === 'CONSTIT' ? `Décision n°` : 'Numéro: ',
        value: window.arret.juridiction === 'CONSTIT' ? `${window.arret.numero} ${window.arret.nature_abrev_cc}` : window.arret.numero
      })
    }
    if (window.arret.resolution_number != null) { data.push({
        key: 'Résolution: ',
        value: window.arret.resolution_number
      })
    }
    if (window.arret.numeros_rapport != null) { data.push({
        key: 'Numéro(s) de rapport: ',
        value: window.arret.numeros_rapport
      })
    }
    this.setState({
      data: data
    })
  }

  render() {
    return (
        (this.state.data.map(el => (
        <div className="decision__header__meta__item">
          <span className="decision__strong">{el.key}</span> 
          {el.value}
        </div>)
      ))
    )
  }
}
