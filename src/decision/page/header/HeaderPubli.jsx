import React, { Component } from 'react';

export class HeaderPubli extends Component {

  state = {
    data: [],
    importance: {
      A: 'Publié au recueil Lebon',
      B: 'Mentionné aux tables du recueil Lebon',
      C: 'Inédit au recueil Lebon',
      'C+': 'Intérêt jurisprudentiel signalé'
    }
  }  

  componentDidMount() {
    const data = []
    if (window.arret.publi_bull != null) { data.push({
        key: 'Importance: ',
        value: window.arret.publi_bull ? 'Publié au bulletin' : 'Inédit',
      })
    }
    if (window.arret.publi_recueil != null) { data.push({
        key: 'Importance: ',
        value: this.state.importance[window.arret.publi_recueil],
      })
    }
    if (window.arret.publi_bull_ref != null) { data.push({
        key: 'Publication: ',
        value: window.arret.publi_bull_ref
      })
    }
    if (window.arret.titre_jo != null) { data.push({
        key: 'Publication: ',
        value: window.arret.titre_jo
      })
    }
    this.setState({
      data: data
    })
  }

  render() {
    return (
        (this.state.data.map(el => (
        <div className="decision__header__meta__item">
          <span className="decision__strong">{el.key}</span> 
          {el.value}
        </div>)
      ))
    )
  }
}
