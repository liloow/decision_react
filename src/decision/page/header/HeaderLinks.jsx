import React, { Component } from 'react';
import { limitString, citationJpExplanation, clean_text } from '../../../utils/utils'

export class HeaderLinks extends Component {

  state = {
    data: [],
  }  

  componentDidMount() {
    const data = []
    if (window.arret.arrets_dec_att != null) { data.push({
        key: '',
        value: ''
      })
    }
    if (window.arret.inpi_lien != null) { data.push({
      key: 'Décision(s) liée(s): ',
      value: (<ul className="decision__header__list">
              <li className="decision__header__list__item">{window.arret.inpi_lien.replace(/; ([^\d])/g,'</li><li>$1')}</li>
            </ul>)
      })
    }
    if (window.parsed_citation_jp != null) { 
      if (window.arret.juridiction === 'CEDH') { data.push({
        key: 'Jurisprudence de Strasbourg: ',
        value: ( <ul><li>{window.parsed_citation_jp}</li></ul>)
      })
      } else { data.push({
        key: 'Précédents jurisprudentiels: ',
        value: ( <ul><li>{window.parsed_citation_jp}</li></ul>)
      })
      }
    }  
    if (window.suivants != null) { data.push({
      key: 'Évolution jurisprudentielle postérieure: ',
      value: window.suivants.map((suivant, i) => (
        <span key={i}>
          <a ng-href={suivant.reference_url} dangerouslySetInnerHTML={limitString(suivant.titre, 80)} />
          <span dangerouslySetInnerHTML={citationJpExplanation(suivant.citation_jp_type)}></span>
        </span>
        ))
      })
    }
    if (window.arret.liens != null) { 
      if (window.arret.juridiction === 'CEDH') { data.push({
        key: 'Références à des textes internationaux: ',
        value: <ul><li>{window.arret.liens.replace(/;/g,'</li><li>')}</li></ul>
      })
    }
      } else if (window.arret.juridiction === 'CJUE') { data.push({
        key: 'Traité: ',
        value: window.arret.liens
      })
      } else { data.push({
        key: 'Textes appliqués: ',
        value: clean_text(window.arret.liens)
      })
    }  
    if (window.arret.issue != null) { data.push({
        key: window.arret.issue.includes(';') ? 'Références à des dispositions du droit interne' : 'Référence à une disposition du droit interne' ,
        value: window.arret.issue.includes(';') ? window.arret.issue.replace(/;/g,'</li><li>') : window.arret.issue
      })
    }
    if (window.arret.rules_of_court != null) { data.push({
      key: window.arret.rules_of_court.includes(';') ? 'Références au règlement de la Cour :' : 'Référence au règlement de la Cour: ' ,
      value: window.arret.rules_of_court.includes(';') ? `Articles ${window.arret.rules_of_court.replace(/;/g,', ')}` : `Article ${window.arret.rules_of_court}`
      })
    }
    if (window.arret.organisations != null) { data.push({
      key: window.arret.organisations.includes(';') ? 'Organisations mentionnées: ' : 'Organisation mentionnée: ' ,
      value: window.arret.organisations.includes(';') ? `Articles ${window.arret.organisations.replace(/;/g,', ')}` : `Article ${window.arret.organisations}`
      })
    }
  }

  render() {
    return (
              (this.state.data.map(el => (
        <div className="decision__header__meta__item">
          <span className="decision__strong">{el.key}</span> 
          {el.value}
        </div>)
      )
    ))
  }
}


