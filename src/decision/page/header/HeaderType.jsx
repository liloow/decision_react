import React, { Component } from 'react';

export class HeaderType extends Component {

  state = {
    data: [],
  }  

  componentDidMount() {
    const data = []
    if (window.arret.type_rec != null) { data.push({
        key: 'Type de recours: ',
        value: window.arret.type_rec 
      })
    }
    if (window.arret.type_decision_cedh != null) { data.push({
        key: 'Type de document: ',
        value: window.arret.type_decision_cedh
      })
    }
    if (window.arret.type_decision_cc != null) { data.push({
        key: 'Type de décision: ',
        value: window.arret.type_decision_cc
      })
    }
    if (window.arret.numeros_rapport != null) { data.push({
        key: 'Numéro(s) de rapport: ',
        value: window.arret.numeros_rapport
      })
    }
    this.setState({
      data: data
    })
  }

  render() {
    return (
        (this.state.data.map(el => (
        <div className="decision__header__meta__item">
          <span className="decision__strong">{el.key}</span> 
          {el.value}
        </div>)
      ))
    )
  }
}
