import React, { Component } from 'react';

export class HeaderReference extends Component {
  render() {
    return (
      <div className="hide_on--print">
        <div className="decision__header__meta__item">
          <span className="decision__strong">
            Référence :
          </span>
          <span className="show_on--mobile decision--print__reference">
              {window.arret.reference_citation}
          </span>
          <br />
          <input className="hide_on--mobile" id="reference_input" type="text" name="copier la référence" readOnly="readonly" value={window.arret.reference_citation + '. Lire en ligne : https://www.doctrine.fr' + window.arret.reference_url} />
          <a className="has-tip button small secondary pointer" data-copy-reference data-type="copy-link" ng-click="decisionVm.copyDecisionLink('clipboard_copy_dropdown', $event)" data-tooltip-placement="bottom" data-tooltip-popup-delay='100' data-tooltip="Copier le lien et la référence (lien public, accessible à tous)">
            <i className="material-icons decision__header__toolbar__button__icon">file_copy</i>
            <span className="decision__header__toolbar__button__label"> 
            <span className="decision__header__toolbar__button__label">Copier la référence</span>            </span>
          </a>
        </div>
      </div>
    );
  }
}


