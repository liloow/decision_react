// @flow

import React, { Fragment } from 'react';
import { DecisionMap } from './DecisionMap';
import { DecisionLayout } from './DecisionLayout';
import { SimilarLawDecisions } from './SimilarLawDecisions';
import type { arrets, comments } from '../types';

type Props = {
  arretInfos: arrets,
  decisionComments: Array<comments>,
  accessPremium: boolean
};

export function Root(props: Props) {
  const document_read_key = window.document_read_key;
  const decisionMap = window.decision_map;
  const decisionMapData = window.decision_map_data;
  let { docId, contenuHtml } = props.arretInfos;
  contenuHtml = props.arretInfos.contenuHtml.replace(
    /<span class="decision_highlight">/g,
    '<span data-doctrine-popover class="decision_highlight">',
  );
  const { decisionComments, accessPremium } = props;
  return (
    <Fragment>
      <DecisionMap decisionMap={decisionMap} decisionMapData={decisionMapData} accessPremium={accessPremium} />
      <DecisionLayout
        docId={docId}
        document_read_key={document_read_key}
        contenuHtml={contenuHtml}
        accessPremium={accessPremium}
        arret={props.arretInfos}
        decisionMapData={decisionMapData}
        decisionComments={decisionComments}
      />
      <SimilarLawDecisions docId={docId} document_read_key={document_read_key} accessPremium={accessPremium} />
    </Fragment>
  );
}
