// @flow

import React, { PureComponent } from "react";
import ReactDOM from "react-dom";
import { TooltipSelect } from "../popover/TooltipSelect";
const popoverRoot: ?HTMLElement = document.getElementById("popover-root");

type Props = {
  tooltipCorrectionY: number,
  tooltipCorrectionX: number,
  selection: ?{},
  val: ?string,
  page: { Y: number, X: number },
  offset: { Y: number, X: number }
};

type State = {
  loaded: boolean,
  val: ?string,
  top: number,
  left: number
};

export class Ghost extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loaded: false,
      val: null,
      top: 0,
      left: 0
    };
  }

  static getDerivedStateFromProps(props: Props, state: State) {
    if (props.val !== state.val) {
      return {
        val: props.val,
        loaded: false,
        ...props
      };
    }
    return null;
  }

  async componentDidUpdate() {
    if (this.props.val && !this.state.loaded) {
      return this.setState({
        val: this.props.val,
        top: this.props.page.Y - this.props.tooltipCorrectionY,
        left: this.props.page.X - this.props.tooltipCorrectionX,
        loaded: true
      });
    }
    return null;
  }

  ghostRef: ?HTMLElement;

  render() {
    if (popoverRoot) {
      return ReactDOM.createPortal(
        <div
          data-doctrine-popover
          id="ghost"
          ref={c => {
            this.ghostRef = c;
          }}
          style={{
            width: "225px",
            fontSize: "0rem",
            height: "100px",
            position: "absolute",
            top: this.state.top,
            left: this.state.left,
            backgroundColor: "transparent",
            zIndex: 105
          }}
        >
          <TooltipSelect {...this.state} />
        </div>,
        popoverRoot
      );
    } return null;
  }
}
