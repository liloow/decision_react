import React, { Component } from 'react';

export class DecisionHeader extends Component {
  render() {
    return (
      <div className="decision__header__top">
        <h1 id="decision-title" className="decision__header__top__title">
          {this.props.shortTitle}
        </h1>
      </div>
    );
  }
}


