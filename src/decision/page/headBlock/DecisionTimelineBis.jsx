import React, { Component } from 'react';
import { french_date_html } from '../../../utils/utils';
import ReactDOM from 'react-dom';
function getYear(date) {
  let year;
  if (date.constructor === Date) year = date.getFullYear();
  else year = new Date(date).getFullYear();
  if (!(year > 0)) year = Number(date.match(/[19,20]{2}[0-9]{2}/)[0]);
  return year;
}

function getScaledX(offstart, decisionDate, startDate, stepLength, initX) {
  let yearsApart = decisionDate - startDate;
  let offset = yearsApart * stepLength / 5;
  return offset + initX + offstart;
}

function checkIfCollide(
  offstart,
  arr,
  currentIndex,
  decisionDate,
  nodePosY,
  boxWidth,
  startDate,
  stepLength,
  initX,
  separatingHeight,
  data,
  cache,
) {
  if (data[currentIndex].Y) return data[currentIndex].Y;
  let nodePosX = getScaledX(offstart, decisionDate, startDate, stepLength, initX);
  let boundaries = {
    left: (nodePosX - boxWidth / 2) > 0 ? (nodePosX - boxWidth / 2) : 1,
    right: (nodePosX + boxWidth / 2) > 0 ? (nodePosX + boxWidth / 2) : 1,
  }
  let multi = Number(checkWithCache(cache, boundaries));
  data[currentIndex].Y = nodePosY - multi * separatingHeight;
  const bounds = { x: boundaries.right, y: data[currentIndex].Y };
  saveToCache(cache, bounds, multi);
  return data[currentIndex].Y;
}

function saveToCache(cache, bounds, floor) {
  cache[floor] = bounds.x;
}

function checkWithCache(cache, bounds) {
  for (let floor of Object.keys(cache)) {
    if (cache[floor] < bounds.left) return floor;
  }
  return Object.keys(cache).length;
}

export class DecisionTimelineBis extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 600,
      height: 250,
      start: { x: 0, y: 0, date: 0 },
      end: { x: 0, y: 0, date: 0 },
      separatingHeight: 50,
      initBoxHeight: 25,
      offstart: 25,
      stepLength: 0,
      cache: {
        0: 0,
        1: 0,
        2: 0,
        3: 0,
      },
    };
  }

  style = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '400px',
  };

  componentDidMount() {
    let startArray = this.props.linksDecisions.map(el => getYear(el.dateDec));
    startArray.push(getYear(this.props.arret.dateDec));
    let minDate = Math.min(...startArray);
    let maxDate = Math.max(...startArray);
    const start = {
      date: minDate + 5 - minDate % 5,
    };
    const end = {
      date: maxDate % 5 === 0 ? maxDate : maxDate - maxDate % 5,
    };
    let numberOfSteps = (end.date - start.date) / 5 - 1;
    start.x = Math.floor(this.state.offstart + this.state.width / (numberOfSteps + 2) / 2);
    start.y = this.state.height;
    end.x = Math.floor(
      this.state.width - this.state.width / (numberOfSteps + 2) / 2 - this.state.offstart,
    );
    end.y = this.state.height;
    let timelineLength = end.x - start.x;
    let stepLength = timelineLength / (numberOfSteps + 1);

    this.data.forEach((entry, i) => {
      document.getElementById('lengthTester').textContent = entry.juridiction;
      let temp = document.getElementById('lengthTester').getBBox();
      document.getElementById('lengthTester').textContent = entry.solutionParsed;
      let temp2 = document.getElementById('lengthTester').getBBox();
      entry.box = Math.max(temp.width, temp2.width * 0.7) + 20;
      entry.bounds =  [temp, temp2].find(
        el => el.width === entry.box - 20 || el.width * 0.7 === entry.box - 20,
      );
    });

    this.setState({
      startArray: startArray,
      minDate: minDate,
      maxDate: maxDate,
      start: start,
      end: end,
      numberOfSteps: numberOfSteps,
      timelineLength,
      stepLength,
    });
  }

  textElem = (
    <g id="backplan">
      {' '}
      <text id="lengthTester" />{' '}
    </g>
  );

  handleHover = e => {
    if (e.type === 'mouseenter') {
      [...e.currentTarget.querySelectorAll('text')].forEach(el => {
        el.setAttribute('fill', 'white');
      });
      [...e.currentTarget.querySelectorAll('path')].forEach(el => {
        el.setAttribute('fill', '#36b');
      });
    }
    if (e.type === 'mouseleave') {
      [...e.currentTarget.querySelectorAll('text')].forEach(el => {
        el.setAttribute('fill', 'black');
      });
      [...e.currentTarget.querySelectorAll('path')].forEach(el => {
        el.setAttribute('fill', 'white');
      });
    }
  };

  data = [...this.props.linksDecisions, this.props.arret]
    .map(el => {
      let entry = {};
      entry.docId = el.docId;
      entry.date = getYear(el.dateDec);
      entry.fullDate = el.dateDec;
      entry.juridiction = `${el.juridiction} ${el.ville || ''}`;
      entry.solutionParsed = el.solutionParsed || '';
      entry.isCurrent = this.props.arret.docId === entry.docId ? true : false;
      return entry;
    })
    .sort((a, b) => a.date - b.date);

  render() {
    const {
      start,
      initBoxHeight,
      separatingHeight,
      offstart,
      stepLength,
      cache,
    } = this.state;
    return (
      this.props.linksDecisions.length > 0 && (
        <div className="container" style={this.style}>
          <svg height="300" width={this.state.width + 60}>
            {this.textElem}
            <line
              x1={offstart}
              y1={this.state.height}
              x2={this.state.width + offstart}
              y2={this.state.height}
              style={{ stroke: 'rgb(0,0,0)', strokeWidth: 3 }}
            />
            {/* start */}
            <circle id="start" cx={offstart + start.x} cy={this.state.height} r="6" />
            <text
              x={this.state.start.x + offstart}
              y={this.state.start.y + initBoxHeight}
              textAnchor="middle"
            >
              {this.state.start.date}
            </text>
            {Array(this.state.numberOfSteps)
              .fill(null)
              .map((el, i) => (
                <g key={i}>
                  <circle
                    key={`step${i}`}
                    cx={offstart + start.x + stepLength * (i + 1)}
                    cy={this.state.height}
                    r="5"
                  />
                  <text
                    x={offstart + start.x + stepLength * (i + 1)}
                    y={this.state.start.y + initBoxHeight}
                    textAnchor="middle"
                  >
                    {this.state.start.date + 5 * (i + 1)}
                  </text>
                </g>
              ))}
            {this.state.numberOfSteps &&
              this.data.map((d, i, arr) => (
                <g
                  data-date={d.date}
                  key={d.docId}
                  onMouseEnter={this.handleHover}
                  onMouseLeave={this.handleHover}
                  style={{ pointerEvents: d.isCurrent ? 'none' : 'auto' }}
                >
                  <a>
                    <circle
                      key={`node${i}`}
                      cx={getScaledX(offstart, d.date, start.date, stepLength, start.x)}
                      cy={this.state.height}
                      r="4"
                    >
                      <title>{french_date_html(new Date(d.fullDate)).replace(/<.+?>/g, '')}</title>
                    </circle>
                  </a>
                  {ReactDOM.createPortal(
                    <line
                      x1={getScaledX(offstart, d.date, start.date, stepLength, start.x)}
                      y1={this.state.height}
                      x2={getScaledX(offstart, d.date, start.date, stepLength, start.x)}
                      y2={checkIfCollide(
                        offstart,
                        arr,
                        i,
                        d.date,
                        this.state.start.y - initBoxHeight,
                        d.box,
                        start.date,
                        stepLength,
                        start.x,
                        separatingHeight,
                        this.data,
                        cache,
                      )}
                      style={{
                        stroke: 'rgb(0,0,0)',
                        strokeWidth: 1,
                        pointerEvents: d.isCurrent ? 'none' : 'auto',
                      }}
                    />,
                    document.getElementById('backplan'),
                  )}
                  <a id={`node${i}`} href={`/d/${d.docId}`} style={{ cursor: 'pointer' }}>
                    <path
                      id={`path${i}`}
                      d={`m ${getScaledX(offstart, d.date, start.date, stepLength, start.x) -
                        d.box / 2} ${checkIfCollide(
                        offstart,
                        arr,
                        i,
                        d.date,
                        this.state.start.y - initBoxHeight,
                        d.box,
                        start.date,
                        stepLength,
                        start.x,
                        separatingHeight,
                        this.data,
                        cache,
                      )} h ${d.box} v -40 h -${d.box} Z`}
                      fill={d.isCurrent ? '#36b' : 'white'}
                      rc="20%"
                      stroke="#36b"
                    />
                    <text
                      x={getScaledX(offstart, d.date, start.date, stepLength, start.x)}
                      y={
                        checkIfCollide(
                          offstart,
                          arr,
                          i,
                          d.date,
                          this.state.start.y - initBoxHeight,
                          d.box,
                          start.date,
                          stepLength,
                          start.x,
                          separatingHeight,
                          this.data,
                          cache,
                        ) - 35
                      }
                      textAnchor="middle"
                      fontSize="1em"
                      id={`span${i}`}
                      dy={d.solutionParsed ? 13 : 20}
                      fill={d.isCurrent ? 'white' : 'black'}
                    >
                      {d.juridiction}
                    </text>
                    {d.solutionParsed && (
                      <text
                        x={getScaledX(offstart, d.date, start.date, stepLength, start.x)}
                        y={
                          checkIfCollide(
                            offstart,
                            arr,
                            i,
                            d.date,
                            this.state.start.y - initBoxHeight,
                            d.box,
                            start.date,
                            stepLength,
                            start.x,
                            separatingHeight,
                            this.data,
                            cache,
                          ) - 8
                        }
                        textAnchor="middle"
                        fontSize="0.7em"
                        fill={d.isCurrent ? 'white' : 'black'}
                        style={{ zIndex: '10' }}
                      >
                        {d.solutionParsed}
                      </text>
                    )}
                  </a>
                </g>
              ))}

            {/* end */}
            <circle id="end" cx={this.state.end.x + offstart} cy={this.state.height} r="6" />
            <text
              x={this.state.end.x + offstart}
              y={this.state.end.y + initBoxHeight}
              textAnchor="middle"
            >
              {this.state.end.date}
            </text>
          </svg>
        </div>
      )
    );
  }
}
