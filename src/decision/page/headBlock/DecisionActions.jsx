import React, { Component } from "react";
import { clipboardJS } from "clipboard";

export class DecisionActions extends Component {
  state = {
    shareMenuIsOpened: false,
    planMenuIsOpened: false,
    style: {
      marginLeft: '0px',
      top: '59px',
      width: '631px',
      position: 'static',
    }
  };

  openShareMenu = e => {
    this.setState({ shareMenuIsOpened: true });
  };
  closeShareMenu = e => {
    this.setState({ shareMenuIsOpened: false });
  };
  openPlanMenu = e => {
    this.setState({ planMenuIsOpened: true });
  };
  closePlanMenu = e => {
    this.setState({ planMenuIsOpened: false });
  };

  copyLink() {
    new clipboardJS('[data-type="copy-link"]', {
      text: () => window.decision_clipboard
    });
  }

  handleDetach = (e) => {
    if (e.type === 'resize' && window.outerWidth < 1115 ) {
      return this.setState({
        style: {
          ...this.state.style,
          position: 'fixed',
          top: 'auto',
          marginLeft: 0,
          height: '49px',
          width: 'calc(100% + 4px)!important',
        }
      })
    }
    if ( (139 - window.scrollY) < 0 && this.state.style.position !== 'fixed') {
      return this.setState({
        style: {
          ...this.state.style,
          position: 'fixed',
        }
      })
    } if (137 - window.scrollY > 0 && this.state.style.position !== 'static') {
      return this.setState({
        style: {
          ...this.state.style,
          position: 'static',
        }
      })
    }   
  }

  componentDidMount() {
    window.addEventListener('scroll', (e) => this.handleDetach, false)
    window.addEventListener('resize', (e) => this.handleDetach, false)
    if (window.outerWidth < 1115) {
      return this.setState({
        style: {
          ...this.state.style,
          position: 'fixed',
          top: 'auto',
          marginLeft: 0,
          height: '49px',
          width: '100vw',
        }
      })
    } return null 
  }
  

  render() {
    const {style} = this.state
    return (
      <div
        className="decision__header__icon navhits"
        id="sticky"
        style={style}
      >
        <div>
          <div
            className="decision__header__icon__share button tiny secondary decision__header__toolbar hide_on--desktop--large  hide_on--tablet hide_on--mobile decision__header__icon__share--selected"
            onMouseOver={e => this.openPlanMenu(e)}
            onMouseLeave={e => this.closePlanMenu(e)}
          >
            <i className="material-icons">list</i>
            <span className=" _on--mobile"> Plan </span>
            <i className="material-icons  _on--mobile">arrow_drop_down</i>
            <div
              className="icon__dropdown__summary__block"
              style={{
                display: this.state.planMenuIsOpened ? "block" : "none"
              }}
            >
              <decision-map>
                <div className="decision__map__empty"> </div>
                <div
                  className="navigation_map ng-scope"
                  ng-controller="decision_map as decisionMapVm"
                  style={{
                    height: "auto",
                    paddingBottom: "6px",
                    position: "absolute",
                    top: "13px",
                    width: "420px",
                    display: "inline-table"
                  }}
                >
                  <div className="decision__map__title">
                    {" "}
                    Plan de la décision{" "}
                  </div>
                  <div
                    className="decision__map"
                    style={{ height: "auto", maxHeight: "388px" }}
                  >
                    <a
                      className="decision__map__subtitle__generic decision__map__subtitle--selected"
                      ng-click="decisionMapVm.goToId('decision-title', 'Titre')"
                      target="_self"
                    >
                      <span className="decision__map__subtitle__generic__label">
                        {" "}
                        Titre{" "}
                      </span>{" "}
                    </a>
                    {this.props.decisionMapData.has_chrono && (
                      <div>
                        <a
                          className={
                            this.state.planMenuIsOpened
                              ? "decision__map__subtitle--selected"
                              : "decision__map__subtitle__generic"
                          }
                          target="_self"
                        >
                          <span className="decision__map__subtitle__generic__label">
                            Chronologie de l’affaire
                          </span>
                        </a>
                        <a
                          className={`decision__map__subtitle__generic decision__map__subtitle--selected ${
                            this.state.planMenuIsOpened
                              ? "decision_title-infos"
                              : ""
                          }`}
                          target="_self"
                        >
                          <span className="decision__map__subtitle__generic__label">
                            Informations
                          </span>
                        </a>
                        <a
                          className={`decision__map__subtitle__generic decision__map__subtitle--selected ${
                            this.state.planMenuIsOpened
                              ? "decision_title-text"
                              : "decision__map__subtitle__generic"
                          }`}
                          target="_self"
                        >
                          <span className="decision__map__subtitle__generic__label">
                            Texte intégral
                          </span>
                        </a>
                      </div>
                    )}
                  </div>
                </div>
                <div className="decision-decision__map__subtitle__genericmap" />
              </decision-map>
            </div>
          </div>
          <div className="decision__header__toolbar button-group tiny secondary no-gaps">
            <a className="button ng-scope" target="_self">
              <i className="material-icons decision__header__toolbar__button__icon">
                create_new_folder
              </i>
              <span className="decision__header__toolbar__button__label">
                {" "}
                Ajouter <span className=" _on--tablet">à mes dossiers</span>
              </span>
            </a>
            <a
              className="button  hide_on--mobile"
              onClick={() => window.print()}
              target="_self"
            >
              <i className="material-icons">print</i>
              <span> Imprimer</span>
            </a>
            <a
              className="has-tip button pointer"
              ng-click="decisionVm.modal.openRestrictedModal('pdf')"
              target="_self"
            >
              <i className="material-icons decision__header__toolbar__button__icon">
                file_download
              </i>
              <span className="decision__header__toolbar__button__label">
                {" "}
                PDF
              </span>
            </a>
          </div>
          <div
            className="decision__header__toolbar button-group tiny secondary no-gaps"
            style={{ marginRight: 0 }}
          >
            <a
              className="has-tip button pointer"
              data-copy-reference
              data-type="copy-link"
              onClick={() => this.copyLink()}
              data-tooltip-placement="bottom"
              data-tooltip-popup-delay="100"
              data-tooltip="Copier le lien et la référence (lien public, accessible à tous)"
              target="_self"
            >
              <i className="material-icons decision__header__toolbar__button__icon">
                file_copy
              </i>
              <span className="decision__header__toolbar__button__label">
                {" "}
                Copier<span className="show-for-xlarge"> la référence</span>
              </span>
            </a>
            <div
              className="button decision__toolbar__button__share decision__header__toolbar__button ng-isolate-scope"
              data-responsive-dropdown=""
              data-show="decisionVm.openShareDropdown()"
              data-="decisionVm.closeShareDropdown()"
              data-delay="200"
              data-close-onclick="true"
              onMouseOver={e => this.openShareMenu(e)}
              onMouseLeave={e => this.closeShareMenu(e)}
            >
              <i className="material-icons decision__header__toolbar__button__icon">
                share
              </i>
              <span className="decision__header__toolbar__button__label show_on--desktop--large show_on--mobile  _on--tablet">
                Partager
              </span>
              <i className="material-icons  _on--mobile">arrow_drop_down</i>

              <div
                className="icon__dropdown__arrow ng- "
                ng-show="decisionVm.show_decision_nav_icon == 'icon__dropdown'"
              />
              <div className="icon__dropdown__block">
                <div
                  className="icon__dropdown"
                  style={{
                    display: this.state.shareMenuIsOpened ? "block" : "none"
                  }}
                >
                  <a
                    className="icon__dropdown__link"
                    ng-href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Fwww.doctrine.fr%2Fd%2FTGI%2FParis%2F1972%2FDE8759599495785446%3Fsource%3Dlinkedin_share&amp;title=TGI%20Paris%2C%2013%20d%C3%A9c.%201972&amp;summary=Attendu%20que%20les%20%C3%A9poux%20Saint-Arroman%20ont%20assign%C3%A9%20le%20Direc%C2%ADteur%20de%20la%20R%C3%A9union%20des%20Mus%C3%A9es%20nationaux%2C%20Rheims%2C%20ancien%20commissaire-priseur%2C%20Lebel%2C%20expert%2C%20et%20Laurin%2C%20commissaire-priseur%2C%20pour%20%3A%201)%20entendre%20prononcer%20la%20nul%C2%ADlit%C3%A9%20de%20la%20vente%20aux%20ench%C3%A8res%E2%80%A6"
                    data-size="large"
                    target="_self"
                    title="Partager sur LinkedIn"
                    href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Fwww.doctrine.fr%2Fd%2FTGI%2FParis%2F1972%2FDE8759599495785446%3Fsource%3Dlinkedin_share&amp;title=TGI%20Paris%2C%2013%20d%C3%A9c.%201972&amp;summary=Attendu%20que%20les%20%C3%A9poux%20Saint-Arroman%20ont%20assign%C3%A9%20le%20Direc%C2%ADteur%20de%20la%20R%C3%A9union%20des%20Mus%C3%A9es%20nationaux%2C%20Rheims%2C%20ancien%20commissaire-priseur%2C%20Lebel%2C%20expert%2C%20et%20Laurin%2C%20commissaire-priseur%2C%20pour%20%3A%201)%20entendre%20prononcer%20la%20nul%C2%ADlit%C3%A9%20de%20la%20vente%20aux%20ench%C3%A8res%E2%80%A6"
                  >
                    <i
                      className="fa fa-linkedin icon__dropdown__link__picto"
                      aria-hidden="true"
                    />
                    Partager sur LinkedIn
                  </a>

                  <a
                    className="icon__dropdown__link"
                    ng-href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fwww.doctrine.fr%2Fd%2FTGI%2FParis%2F1972%2FDE8759599495785446%3Fsource%3Dtwitter_share&amp;text=TGI%20Paris%2C%2013%20d%C3%A9c.%201972"
                    data-size="large"
                    target="_self"
                    title="Partager sur Twitter"
                    href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fwww.doctrine.fr%2Fd%2FTGI%2FParis%2F1972%2FDE8759599495785446%3Fsource%3Dtwitter_share&amp;text=TGI%20Paris%2C%2013%20d%C3%A9c.%201972"
                  >
                    <i
                      className="fa fa-twitter icon__dropdown__link__picto"
                      aria-hidden="true"
                    />
                    Partager sur Twitter
                  </a>

                  <a
                    className="icon__dropdown__link"
                    id="facebookDecisionShare"
                    title="Partager sur Facebook"
                    target="_self"
                  >
                    <i
                      className="fa fa-facebook icon__dropdown__link__picto"
                      aria-hidden="true"
                    />
                    Partager sur Facebook
                  </a>

                  <a
                    className="icon__dropdown__link"
                    href="mailto:?subject=D%C3%A9cision%20int%C3%A9ressante%20%3A%20TGI%20Paris%2C%2013%20d%C3%A9c.%201972&amp;body=Voici%20une%20d%C3%A9cision%20int%C3%A9ressante%20%C3%A0%20lire%20sur%20Doctrine.fr%20%3A%20https%3A%2F%2Fwww.doctrine.fr%2Fd%2FTGI%2FParis%2F1972%2FDE8759599495785446"
                    rel="external"
                    title="Partager par email"
                    target="_self"
                  >
                    <i
                      className="fa fa-envelope icon__dropdown__link__picto"
                      aria-hidden="true"
                    />
                    Partager par email
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
