import React, { Component } from "react";
import vis from "vis";

export class DecisionTimeline extends Component {
  
  async componentDidMount() {
    const timeline_items = new vis.DataSet(window.datasetarray);
    const container = document.getElementById("visualization");
    const options = {
      // Configuration for the Timeline
      zoomable: false,
      moveable: false,
      // moveable: true,
      selectable: false,
      zoomMin: 1000000000,
      maxHeight: "400px",
      minHeight: "170px",
      showCurrentTime: false
    };
    new vis.Timeline(container, timeline_items, options); // Create a Timeline
    // to remove scroll
    const centerPanel = document.querySelector("div.vis-panel.vis-center");
    centerPanel.addEventListener(
      "mousewheel",
      function(event) {
        event.stopPropagation();
      },
      true
    );
  }

  render() {
    return (
      (this.props.linksDecisions.length > 0) && (
      <div className="decision__timeline hide_on--print" id="decision_timeline">
        <div className="decision__integral" id="decision_title-chrono">
          <i className="material-icons decision__integral__icon">access_time</i>
          <span>Chronologie de l’affaire</span>
          <div
            className="decision__timeline__print show_on--print"
          >
            <div ng-hide="$first" className="decision__timeline__print__after">
              >
            </div>
            <div
              className="decision__timeline__print__block"
            >
              <span
                className="decision__timeline__print__block__content"
                ng-bind-html="item.content"
              />
              <span
                className="decision__timeline__print__block__start"
                ng-bind="item.readable_start"
              />
            </div>
          </div>
        </div>
          <div id="visualization" />
      </div>
    ));
  }
}


