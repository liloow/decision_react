import React from "react";

export function DecisionKeywords(props) {
  const { keywords } = props;
  return (
    <ul className="decision__header__subtitle__inner">
      {keywords.map((keyword, i, arr) => (
        <li key={i} className="decision__header__subtitle__item">
          <a
            className="decision__header__subtitle__item__text"
            href={`/?q=${keyword}&source=search_by_keyword`}
            target="_self"
          >
            {keyword}
          </a>
          {i !== arr.length - 1 ? (
            <span className="decision__header__subtitle__item__separator">
              ·
            </span>
          ) : (
            ""
          )}
        </li>
      ))}
    </ul>
  );
}
