import React, { Component } from "react";
import { fetchSimilarLawDecisions } from "../api";

export class SimilarLawDecisions extends Component {
  state = {
    similarDecisions: [],
  };
  async componentDidMount() {
    const similarDecisions = (await fetchSimilarLawDecisions(
      this.props.docId,
      this.props.document_read_key
    )).data;
    this.setState({ similarDecisions: similarDecisions.simlawdecisions });
  }
  render() {
    const hasDescisions = this.state.similarDecisions.length > 0;
    return (
      hasDescisions && (
        <div
          className="reco__law ng-scope"
          id="reco-law"
          ng-controller="decision_similarlaw as decisionSimilarlawVm"
          ng-show="decisionSimilarlawVm.simlawdecisions.length > 0"
        >
          <div className="reco__law" id="reco-law">
            <div className="reco__law__title">
              <i className="material-icons reco__law__title__icon">
                account_balance
              </i>
              Décisions citant les mêmes articles de loi
            </div>
            <div className="reco__law__block">
              {this.state.similarDecisions.map(similarDecision => (
                <div
                  key={similarDecision.docId}
                  className="reco__law__block__subblock"
                >
                  <div className="reco__law__block__subblock__item">
                    <div className="reco">
                      <div className="reco__header">
                        <a target="_self">
                          <span
                            className="reco__title"
                            dangerouslySetInnerHTML={{
                              __html: similarDecision.titre
                            }}
                          />
                        </a>
                        <div className="reco__subtitle">
                          <span>
                            <span className="excerpt__subtitle__title">
                              Décision attaquée&nbsp;:{" "}
                            </span>
                            <span className="excerpt__subtitle__tag ng-binding">
                              {similarDecision.arretDecAtt}
                            </span>
                          </span>
                          <span>
                            <span className="excerpt__subtitle__tag__point">
                              |
                            </span>
                            <span className="excerpt__subtitle__title">
                              Dispositif&nbsp;:{" "}
                            </span>
                            <span className="excerpt__subtitle__tag ng-binding">
                              {similarDecision.solution}
                            </span>
                          </span>
                        </div>
                      </div>
                      <ul className="excerpt__subtitle__inner">
                        {similarDecision.htmlKeywords.map(
                          (keyword, i, arr) => (
                            <li key={i} className="excerpt__subtitle__item">
                              {keyword}{" "}
                              {i !== arr.length - 1 ? (
                                <span className="excerpt__subtitle__item__separator">
                                  ·
                                </span>
                              ) : (
                                ""
                              )}
                            </li>
                          )
                        )}
                      </ul>
                      <div
                        className="excerpt__subtitle__comments__relative has-tip"
                        data-tooltip="{similarDecision.number_of_comments} auteurs ont commenté cette décision"
                        data-tooltip-append-to-body="true"
                        data-tooltip-placement="top"
                        data-tooltip-popup-delay="100"
                      >
                        <span>
                          {similarDecision.numberOfComments} commentaires
                        </span>
                      </div>
                      {similarDecision.similarLawArticles.map((law, i) => (
                        <div
                          key={i}
                          className="reco__law__block__subblock__article"
                        >
                          <i className="material-icons reco__law__block__subblock__article__icon">
                            account_balance
                          </i>
                          <a
                            className="reco__law__block__subblock__article__title ng-binding"
                            ng-href="/?q=Article%201110%20du%20Code%20civil&amp;law_article_id=LEGIARTI000006436121&amp;source=decision_similarlaw"
                            target="_self"
                            href="/?q=Article%201110%20du%20Code%20civil&amp;law_article_id=LEGIARTI000006436121&amp;source=decision_similarlaw"
                          >
                            {law.title}
                          </a>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      )
    );
  }
}
