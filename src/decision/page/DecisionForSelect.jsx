// @flow

import React, { Component } from "react";
import { PopoverFactory } from "../../popover/PopoverFactory";
import { Ghost } from "./Ghost";
import type { arrets } from "../types";

type Props = {|
  docId: string,
  document_read_key: string,
  contenuHtml: string,
  accessPremium: boolean,
  arret: arrets
|};

type State = {
  tooltipCorrectionY: number,
  tooltipCorrectionX: number,
  selection: ?{},
  val: ?string,
  page: { Y: number, X: number },
  offset: { Y: number, X: number }
};

type selectEvent = {
  path: Array<HTMLElement>,
  pageY: number,
  pageX: number,
  offsetX: number,
  offsetY: number
};

export class DecisionForSelect extends Component<Props, State> {
  state = {
    tooltipCorrectionY: 113,
    tooltipCorrectionX: 112.5,
    selection: null,
    val: null,
    page: { Y: 0, X: 0 },
    offset: { Y: 0, X: 0 }
  };

  componentDidMount() {
    document.addEventListener(
      "mouseup",
      (e: MouseEvent) => this.handleEvent(e),
      false
    );
  }

  handleEvent = (e: MouseEvent) => {
    console.log(e);
    if (
      e.srcElement.parentElement &&
      e.srcElement.parentElement.id.includes("decision_text")
    ) {
      const selection = document.getSelection();
      return this.setState({
        selection: selection,
        val: selection ? selection.toString() : null,
        page: { Y: e.pageY, X: e.pageX },
        offset: { X: e.offsetX, Y: e.offsetY }
      });
    }
    return this.setState({
      selection: null,
      val: null
    });
  };

  render() {
    return (
      <div className="decision--for-select" id="decision_text">
        <div className="decision__integral" id="decision_title-text">
          <i className="material-icons decision__integral__icon">subject</i>
          <span>Texte intégral</span>
        </div>
        <PopoverFactory>
          <div
            className="decision__body decision__body__table"
            id="decision_text_content"
            dangerouslySetInnerHTML={{ __html: this.props.contenuHtml }}
          />
          <Ghost {...this.state} />
        </PopoverFactory>
      </div>
    );
  }
}
