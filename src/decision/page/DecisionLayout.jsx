// @flow

import React from "react";
import { DecisionContent } from "./DecisionContent";
import { DecisionAside } from "./DecisionAside";
import type { arrets, comments } from '../types'

type Props = {|
  docId: string,
  document_read_key: string,
  contenuHtml: string,
  accessPremium: boolean,
  arret: arrets,
  decisionComments: Array<comments>,
  decisionMapData: {
    has_summary: boolean,
    has_chrono: boolean
  }
|};

export function DecisionLayout(props: Props) {
  return (
    <div className="decision__layout" id="DecisionLayout">
      <DecisionContent decisionComments={props.decisionComments} {...props} />
      <DecisionAside
        docId={props.docId}
        document_read_key={props.document_read_key}
        decisionComments={props.decisionComments}
      />
    </div>
  );
}
