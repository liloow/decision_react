// @flow

import React from "react";
import type { comments } from '../../types'

type Props = {
  showOnTablet: boolean,
  decisionComments: Array<comments>
};

export function CommentsArticles(props: Props) {
  return (
    <div className={props.showOnTablet ? "show_on--tablet" : "hide_on--tablet"}>
      <div className="decision__aside__component">
        <div className="decision__aside__block__title__reco">
          <i className="material-icons reco__tag__icon__title">comment</i>
          <span>{`Commentaires et articles `}</span>
          <span className="decision__aside__block__title__reco__caption">
            sur cette affaire
          </span>
        </div>
        <div className="decision__aside__block__list">
          {props.decisionComments.map(comment => (
            <div key={comment.id}>
              <a className="decision__aside__block__comment" target="_self">
                <div>
                  <span className="decision__aside__block__link">
                    {comment.dateHtml && (
                      <span className="decision__aside__block__date">
                        <span
                          dangerouslySetInnerHTML={{ __html: comment.dateHtml }}
                        />{" "}
                        –
                      </span>
                    )}
                    {comment.auteur && (
                      <span
                        className="decision__aside__block__author"
                        dangerouslySetInnerHTML={{
                          __html: ` ${comment.auteur} `
                        }}
                      />
                    )}
                    {comment.niceSource && (
                      <span>
                        {comment.auteur && (
                          <span className="decision__aside__block__date">
                            –
                          </span>
                        )}
                        <span
                          className="decision__aside__block__date"
                          dangerouslySetInnerHTML={{
                            __html: ` ${comment.niceSource}`
                          }}
                        />
                      </span>
                    )}
                  </span>
                  <i className="material-icons decision__aside__block__link__icon">
                    launch
                  </i>
                  <span className="decision__aside__block__link">
                    <span
                      className="decision__aside__block__link__title"
                      dangerouslySetInnerHTML={{ __html: comment.titre }}
                    />
                  </span>
                </div>
              </a>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
