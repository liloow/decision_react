// @flow

import React, { Component } from "react";
import { fetchSimilarDecisions } from "../../api";
import type { decisions } from '../../types'

type State = {
  similarDecisions: Array<decisions>
};

type Props = {
  docId: string,
  document_read_key: string
};

export class SimilarDecisions extends Component<Props,State> {
  state = {
    similarDecisions: []
  };

  async componentDidMount() {
    const similarDecisions = (await fetchSimilarDecisions(
      this.props.docId,
      this.props.document_read_key
    )).data;
    this.setState({
      similarDecisions: similarDecisions.simdecisions
    });
  }

  render() {
    const hasDescisions = this.state.similarDecisions.length > 0;
    return (
      hasDescisions && (
        <div ng-show="decisionRecoVm.simdecisions.length > 0" className="">
          <div className="decision__aside__block__title__reco hide_on--tablet">
            <i className="material-icons reco__tag__icon__title">adjust</i>
            <span>Décisions sur des thèmes proches</span>
          </div>
          <div className="decision__aside__block__content">
            <span className="reco__id" id="sim_decisions" />
            <span className="reco__tag__title--sim show_on--tablet">
              <i className="material-icons reco__tag__icon">adjust</i>
              Décisions sur des thèmes proches
            </span>
            {this.state.similarDecisions.slice(0, 3).map(referenceDecision => (
              <div key={referenceDecision.docId} className="">
                <a className="reco">
                  <div className="reco__header">
                    <span
                      className="reco__title"
                      dangerouslySetInnerHTML={{
                        __html: referenceDecision.titre
                      }}
                    />
                    <div className="reco__subtitle">
                      <span>
                        <span className="excerpt__subtitle__title">
                          Décision attaquée&nbsp;:{" "}
                        </span>
                        <span className="excerpt__subtitle__tag ng-binding">
                          {referenceDecision.arretDecAtt}
                        </span>
                      </span>
                      <span>
                        <span className="excerpt__subtitle__tag__point">|</span>
                        <span className="excerpt__subtitle__title">
                          Dispositif&nbsp;:{" "}
                        </span>
                        <span className="excerpt__subtitle__tag ng-binding">
                          {referenceDecision.solution}
                        </span>
                      </span>
                    </div>
                  </div>
                  <ul className="excerpt__subtitle__inner">
                    {referenceDecision.htmlKeywords.map((keyword, i, arr) => (
                      <li key={i} className="excerpt__subtitle__item">
                        {keyword}
                        {i !== arr.length - 1 ? (
                          <span className="excerpt__subtitle__item__separator">
                            ·
                          </span>
                        ) : (
                          ""
                        )}
                      </li>
                    ))}
                  </ul>
                </a>
              </div>
            ))}
          </div>
        </div>
      )
    );
  }
}
