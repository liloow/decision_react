// @flow

import React, { Component } from "react";
import { fetchReferenceDecisions } from "../../api";
import type { decisions } from '../../types'

type Props = {
  docId: string,
  document_read_key: string
};

type State = {|
  referenceDecisions: Array<decisions>
|};

export class ReferenceDecisions extends Component<Props, State> {
  state = {
    referenceDecisions: []
  };

  async componentDidMount() {
    const referenceDecisions = (await fetchReferenceDecisions(
      this.props.docId,
      this.props.document_read_key
    )).data;
    this.setState({
      referenceDecisions: referenceDecisions.refdecisions
    });
  }

  render() {
    const hasDescisions = this.state.referenceDecisions.length > 0;
    return (
      hasDescisions && (
        <div
          className="decision__aside__block ng-scope"
          ng-controller="decision_recomandations as decisionRecoVm"
          ng-show="decisionRecoVm.quoted_with_decisions.length > 0 || decisionRecoVm.simdecisions.length > 0 || decisionRecoVm.refdecisions.length > 0 "
        >
          <div className="decision__aside__block__title__reco hide_on--tablet">
            <i className="material-icons reco__tag__icon__title">star_rate</i>
            <span>{`Décisions de référence `}</span>
            <span className="decision__aside__block__title__reco__caption">
              sur les mêmes thèmes
            </span>
          </div>
          <div className="decision__aside__block__content">
            <span className="reco__id" id="ref_decisions" />
            <span className="reco__tag__title--ref show_on--tablet">
              <i className="material-icons reco__tag__icon">star_rate</i>
              Décisions de référence sur les mêmes thèmes
            </span>
            {this.state.referenceDecisions
              .slice(0, 3)
              .map(referenceDecision => (
                <div key={referenceDecision.docId} className="">
                  <a className="reco">
                    <div className="reco__header">
                      <span
                        className="reco__title"
                        dangerouslySetInnerHTML={{
                          __html: referenceDecision.titre
                        }}
                      />
                      <div className="reco__subtitle">
                        <span>
                          <span className="excerpt__subtitle__title">
                            Décision attaquée&nbsp;:{" "}
                          </span>
                          <span className="excerpt__subtitle__tag ng-binding">
                            {referenceDecision.arretDecAtt}
                          </span>
                        </span>
                        <span>
                          <span className="excerpt__subtitle__tag__point">
                            |
                          </span>
                          <span className="excerpt__subtitle__title">
                            Dispositif&nbsp;:{" "}
                          </span>
                          <span className="excerpt__subtitle__tag ng-binding">
                            {referenceDecision.solution}
                          </span>
                        </span>
                      </div>
                    </div>
                    <ul className="excerpt__subtitle__inner">
                      {referenceDecision.htmlKeywords.map((keyword, i, arr) => (
                        <li key={i} className="excerpt__subtitle__item">
                          {keyword}
                          {i !== arr.length - 1 ? (
                            <span className="excerpt__subtitle__item__separator">
                              ·
                            </span>
                          ) : (
                            ""
                          )}
                        </li>
                      ))}
                    </ul>
                    <div
                      className="excerpt__subtitle__comments__relative has-tip"
                      data-tooltip="{similarDecision.number_of_comments} auteurs ont commenté cette décision"
                      data-tooltip-append-to-body="true"
                      data-tooltip-placement="top"
                      data-tooltip-popup-delay="100"
                    >
                      <span>
                        {referenceDecision.numberOfComments} commentaires
                      </span>
                    </div>
                  </a>
                </div>
              ))}
          </div>
        </div>
      )
    );
  }
}
