// @flow

import React from "react";

export function DidYouKnow() {
  return (
    <div className="decision__aside__block hide_on--tablet">
      <div className="decision__aside__block__title">
        <span>Le saviez-vous ?</span>
      </div>
      <div className="decision__aside__block__content">
        <div className="decision__aside__block__content__home">
          <span className="decision__aside__block__content__home__text">
            Retrouvez les derniers commentaires recommandés pour vous par notre
            intelligence artificielle.
          </span>
          <a
            className="decision__aside__block__content__home__cta"
            ng-click="decisionVm.modal.openRestrictedModal('decision_reco_home')"
            target="_self"
          >
            Voir les derniers commentaires
          </a>
        </div>
      </div>
    </div>
  );
}
