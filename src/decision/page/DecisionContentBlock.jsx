// @flow

import React from 'react';
import { DecisionHeader } from './headBlock/DecisionHeader';
import { DecisionKeywords } from './headBlock/DecisionKeywords';
import { DecisionActions } from './headBlock/DecisionActions';
import { DecisionTimeline } from './headBlock/DecisionTimeline';
import { DecisionTimelineBis } from './headBlock/DecisionTimelineBis';
import { DecisionInfos } from './DecisionInfos';
import type { arrets, comments } from '../types';

type Props = {|
  docId: string,
  document_read_key: string,
  contenuHtml: string,
  accessPremium: boolean,
  arret: arrets,
  decisionMapData: {
    has_summary: boolean,
    has_chrono: boolean
  },
  decisionComments: Array<comments>
|};

export function DecisionContentBlock(props: Props) {
  const { shortTitle, keywords, linksDecisions } = props.arret;
  return (
    <div className="decision__content__block" id="decision__content__block">
      <DecisionHeader shortTitle={shortTitle} />
      <DecisionKeywords keywords={keywords} />
      <DecisionActions decisionMapData={props.decisionMapData} />
      <DecisionTimeline linksDecisions={linksDecisions} />
      <DecisionTimelineBis linksDecisions={linksDecisions} arret={props.arret} />
      <span className="decision__integral" id="decision_title-infos">
        <i className="material-icons decision__integral__icon">info_outline</i>
        <span>Informations</span>
      </span>
      <DecisionInfos arret={props.arret} />
    </div>
  );
}
