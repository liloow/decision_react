// @flow

import React, { Component } from "react";
import ReacDOM from "react-dom";
import { ModalMoreLikeThis } from "./ModalMoreLikeThis";
const modalRoot: ?HTMLElement = document.getElementById("modal-root");

type Props = {
  data: {
    high_hits: Array<{
      reference_url: string,
      titre: string
    }>,
    low_hits: Array<{
      reference_url: string,
      titre: string
    }>
  },
  onRequestClose: Function,
  title: string,
  value: string
};

type State = {
  scroll: number,
  val: ?string,
  highlightedData: {
    high_hits: Array<{
      reference_url: string,
      titre: string
    }>,
    low_hits: Array<{
      reference_url: string,
      titre: string
    }>
  }
};

export class ModalHigher extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      scroll: 0,
      val: null,
      highlightedData: window.fakeMorelikethis
    };
  }

  componentDidMount() {
    document.getElementsByTagName("html")[0].style.overflow = "hidden";
    window.addEventListener(
      "keydown",
      e => {
        if (e.key === "Escape") this.close(e);
      },
      false
    );
    // api call ...
  }

  componentWillUnmount() {
    document.getElementsByTagName("html")[0].style.overflow = "auto";
    window.removeEventListener(
      "keydown",
      e => {
        if (e.key === "Escape") this.close(e);
      },
      false
    );
  }

  close = (e: Event) => {
    return this.props.onRequestClose();
  };

  render() {
    if (modalRoot) {
      return ReacDOM.createPortal(
        <section className="modal_overlay">
          <div
            className="fullscreen"
            onClick={this.close}
            style={{ top: window.scrollY }}
          >
            <div className="modal-wrapper" style={{ maxWidth: "610px" }}>
              <header
                className="small-modal__action__header"
                id="#modal_header"
                style={{ backgroundColor: "#fff" }}
              >
                <span className="small-modal__action__title">
                  {this.props.title}
                </span>
                <a data-tooltip="Close">
                  <i
                    className="material-icons small-modal__close"
                    onClick={this.close}
                  >
                    close
                  </i>
                </a>
              </header>
              <div className="modal_quote_block">
                <p className="modal_quote_block_text">{this.props.value}</p>
              </div>
              <content className="template_container_block">
                {this.state.highlightedData && (
                  <ModalMoreLikeThis data={this.state.highlightedData} />
                )}
              </content>
            </div>
          </div>
        </section>,
        modalRoot
      );
    }
    return null;
  }
}
