// @flow

import React, { Component, createContext } from "react";

const ModalContext = createContext({
  component: null,
  props: {},
  showModal: () => {},
  hideModal: () => {}
});

type Props = {
  children: {}
};

type State = {
  component: ?React$Component<{}>,
  props: {}
};

export class ModalProvider extends Component<Props, State> {
  showModal = (component: React$Component<{}>, props: Props) => {
    this.setState({
      component,
      props
    });
  };

  hideModal = () =>
    this.setState({
      component: null,
      props: {}
    });

  state = {
    component: null,
    props: {},
    showModal: this.showModal,
    hideModal: this.hideModal
  };

  render() {
    return (
      <ModalContext.Provider value={this.state}>
        {this.props.children}
      </ModalContext.Provider>
    );
  }
}

export const ModalConsumer = ModalContext.Consumer;
