// @flow

import React, { Component } from "react";

type Props = {
  data: {
    high_hits: Array<{
      reference_url: string,
      titre: string
    }>,
    low_hits: Array<{
      reference_url: string,
      titre: string
    }>
  }
};

type State = {
  loaded: boolean
};

export class ModalMoreLikeThis extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loaded: false
    };
  }

  componentDidMount() {
    if (this.props.data) {
      setTimeout( () =>
        this.setState({
          loaded: true
        }),
        1200
      ); // simulate api latency
    }
  }

  render() {
    return (
      <div className="more-like modal-body">
        {!this.state.loaded && (
          <div className="more-like__spinner ng-hide">
            <i className="fa fa-spinner fa-spin" />
          </div>
        )}
        {this.state.loaded && (
          <div className="more-like__body">
            <div className="more-like__list">
              <p className="more-like__list__title">Décisions de référence</p>
              {this.props.data.high_hits.map((hit, i) => (
                <div className="more-like__list__content" key={i}>
                  <a
                    className="more-like__list__content__title ng-binding"
                    href={`https://wwww.doctrine.fr${hit.reference_url}`}
                    target="_blank"
                    dangerouslySetInnerHTML={{ __html: hit.titre }}
                    rel="noopener noreferrer"
                  />
                </div>
              ))}
            </div>
            <div className="more-like__list">
              <p className="more-like__list__title">Décisions de référence</p>
              {!this.props.data.high_hits.length && (
                <p className="more-like__list__empty ng-scope">
                  Aucun résultat
                </p>
              )}
              {this.props.data.low_hits.map((hit, i) => (
                <div className="more-like__list__content" key={i}>
                  <a
                    className="more-like__list__content__title ng-binding"
                    href={`https://wwww.doctrine.fr${hit.reference_url}`}
                    target="_blank"
                    dangerouslySetInnerHTML={{ __html: hit.titre }}
                    rel="noopener noreferrer"
                  />
                </div>
              ))}
              {!this.props.data.low_hits.length && (
                <p className="more-like__list__empty ng-scope">
                  Aucun résultat
                </p>
              )}
            </div>
          </div>
        )}
      </div>
    );
  }
}
