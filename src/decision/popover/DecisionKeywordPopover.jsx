import React, { Component } from "react";
import { fetchFicheInfo } from "../api";

export class DecisionKeywordPopover extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fiche: {}
    };
  }

  async componentDidMount() {
    return this.setState({
      fiche: (await fetchFicheInfo(this.props.value)).data
    });
  }

  render() {
    return (
      <a
        className="doctrine-popover"
        target="_self"
        href={`/?q=${this.props.value}`}
      >
        <span className="doctrine-popover__content">
          <i className="material-icons search_card__title__icon">explore</i>
          <div className="doctrine-popover__title search_card__title">
            {this.props.value}
          </div>
        </span>
        {this.state.fiche.text &&
          (window.access_premium && (
            <span className="doctrine-popover__content">
              <span
                dangerouslySetInnerHTML={{ __html: this.state.fiche.text }}
              />
              <span className="doctrine-popover__link">Lire la suite...</span>
            </span>
          ))}
        {!window.access_premium && (
          <span className="doctrine-popover__content">
            <button className="doctrine-popover__btn">Voir plus</button>
          </span>
        )}
      </a>
    );
  }
}
