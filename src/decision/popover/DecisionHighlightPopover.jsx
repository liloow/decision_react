import React, { Component } from "react";
import { ModalHigher } from "../modal/ModalHigher";
import { ModalConsumer } from "../modal/ModalContext";

// import { ModalMoreLikeThis } from './ModalMoreLikeThis'

export class DecisionHighlightPopover extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Recherche par extraits"
    };
  }

  async componentDidMount() {
    this.setState({
      value: this.props.value
    });
  }

  render() {
    return (
      <div className="decision-highlight-popover popover-list">
        <div className="popover-list__item popover-list__item--label">
          Extrait cité de nombreuses fois
        </div>
        <ModalConsumer>
          {({ showModal }) => (
            <button
              className="popover-list__item popover-list__item--button"
              onClick={() =>
                showModal(ModalHigher, { ...this.state }, { ...this.props })
              }
            >
              Voir les décisions similaires
            </button>
          )}
        </ModalConsumer>
      </div>
    );
  }
}
