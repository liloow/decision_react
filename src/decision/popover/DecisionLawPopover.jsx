import React, { Component } from "react";
import { fetchLawInfo } from "../api";

export class DecisionLawPopover extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lawData: null
    };
  }

  async componentDidMount() {
    return this.setState({
      lawData: (await fetchLawInfo(this.props.value)).data
    });
  }

  render() {
    return (
      <a className="doctrine-popover">
        <i className="material-icons search_card__title__icon">
          account_balance
        </i>
        {true && (
          <div
            ref={this.refCallback}
            className="doctrine-popover__title search_card__title--law"
          >
            {this.props.href
              .match(/(Article).+()(?=&)/)[0]
              .replace(/%20/g, " ")}
          </div>
        )}
        {window.access_premium &&
          this.state.lawData && (
            <div>
              <p
                className="doctrine-popover__content ng-binding"
                dangerouslySetInnerHTML={{
                  __html: this.state.lawData.textShort
                }}
              />
            </div>
          )}
      </a>
    );
  }
}
