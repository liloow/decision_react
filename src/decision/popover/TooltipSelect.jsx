import React, { Component } from "react";
import { ModalHigher } from "../modal/ModalHigher";
import { ModalConsumer } from "../modal/ModalContext";
import { copy } from "clipboard";

export class TooltipSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      docId: window.arret.doc_id,
      docKey: window.document_read_key,
      val: null,
      loaded: false,
      data: null,
      flashPopup: false,
      modalActive: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.val !== state.val) {
      return {
        data: window.fakeMorelikethis,
        loaded: false,
        val: ""
      };
    } else return null;
  }

  async componentDidUpdate() {
    // let data
    if (this.props.val && !this.state.loaded) {
      //   try {
      //    data = await moreLikeThis(this.props.val, this.state.docId, this.state.docKey)
      //   } catch (e)  {
      //   console.log(e)
      //   data = window.fakeMorelikethis//.json()
      //   }
      return this.setState({
        val: this.props.val,
        data: window.fakeMorelikethis,
        loaded: true
      });
    }
  }

  copyTextInFolder = () =>
    this.setState({ modalActive: true, modalTemplate: null });

  copyTextReference = () => {
    const snip = `« ${this.state.val} »

    ${window.arret.reference_citation}`;

    return copy(snip);
  };

  render() {
    return (
      <div
        className="decision__tooltip__share hide_on--tablet"
        id="decision__tooltip__share"
        style={{
          display: this.state.val ? "inline-block" : "none",
          transform: "scale(1)",
          opacity: "1",
          width: "225px",
          height: "100px",
          marginTop: 0
        }}
      >
        <a
          className="decision__tooltip__share--elem"
          onClick={e => this.copyTextInFolder(e)}
        >
          <i className="material-icons decision__tooltip__share__icon">
            create_new_folder
          </i>
          Copier dans un dossier
        </a>
        <hr className="decision__tooltip__share__separator" />
        <a
          className="decision__tooltip__share--elem"
          id="decision__tooltip__share__reference"
          onClick={() => this.copyTextReference()}
        >
          <i className="material-icons decision__tooltip__share__icon">
            content_copy
          </i>
          Copier avec la référence
        </a>
        {this.state.flashPopup && (
          <div ng-if="visible" className="folders-actions__copy">
            Extrait copié dans le presse-papier <br />
            (ctrl + V pour coller)
          </div>
        )}
        <hr className="decision__tooltip__share__separator" />
        <ModalConsumer>
          {({ showModal }) => (
            <a
              className="decision__tooltip__share--elem"
              id="decision__tooltip__share__more"
              onClick={() =>
                showModal(
                  ModalHigher,
                  { value: this.state.val, title: "Recherche par extraits" },
                  { ...this.state },
                  { ...this.props }
                )
              }
            >
              <i className="material-icons decision__tooltip__share__icon">
                lightbulb_outline
              </i>
              Voir les décisions similaires
            </a>
          )}
        </ModalConsumer>
      </div>
    );
  }
}
