import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
// import { TooltipKeyword } from './utils/TooltipKeyword'

ReactDOM.render(<App />, document.getElementById('root'));
