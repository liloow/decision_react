// @flow

import * as React from "react";
import { isTouchDevice } from "../common/domUtils";
import { DecisionKeywordPopover } from "../decision/popover/DecisionKeywordPopover";
import { DecisionLawPopover } from "../decision/popover/DecisionLawPopover";
import { DecisionHighlightPopover } from "../decision/popover/DecisionHighlightPopover";
import { Popover } from "./Popover";
import { CursorTracker } from "./cursorTracker";
import type { CursorTrackerType } from "./types";

type Props = {|
  children: React.Node | Array<React.Node>
|};

type State = {|
  Components: Array<React.Node>
|};

export class PopoverFactory extends React.Component<Props, State> {
  state = { Components: [] };

  componentDidMount() {
    this.buildPopovers();
  }

  componentDidUpdate(prevProps: Props) {
    if (
      Array.isArray(prevProps.children) &&
      Array.isArray(this.props.children) &&
      prevProps.children.length !== this.props.children.length
    ) {
      this.buildPopovers();
    }
  }

  componentWillUnmount() {
    if (this.cursorTracker) {
      this.cursorTracker.removeListener();
    }
  }

  factoryRef: ?HTMLDivElement;
  cursorTracker: CursorTrackerType;

  buildPopovers() {
    if (this.factoryRef) {
      const popovers = this.factoryRef.querySelectorAll(
        "[data-doctrine-popover]"
      );
      this.cursorTracker = CursorTracker.getInstance();
      const Components = [];
      [].forEach.call(popovers, (element, index) => {
        Components.push(
          <Popover
            key={index}
            element={element}
            getCursorPosition={this.cursorTracker.getCursorPosition}
          >
            {element.getAttribute("class").includes("fichekeywordlink") && (
              <DecisionKeywordPopover
                value={element.textContent}
              />
            )}
            {element.getAttribute("class").includes("legilink") && (
              <DecisionLawPopover
                href={element.href}
                value={element.href.slice(-20)}
              />
            )}
            {element.getAttribute("class").includes("decision_highlight") && (
              <DecisionHighlightPopover value={element.textContent} />
            )}
          </Popover>
        );
      });
      this.setState({ Components });
    }
  }

  render() {
    const { Components } = this.state;
    return isTouchDevice() ? (
      this.props.children
    ) : (
      <div
        ref={ref => {
          this.factoryRef = ref;
        }}
      >
        {Components.length > 0 && Components} {this.props.children}
      </div>
    );
  }
}
