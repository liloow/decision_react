// @flow

export function eventsHandler({
    linkElement,
    show,
    hide,
}: {
    linkElement: HTMLElement,
    show: Function,
    hide: Function,
}) {
    let enterTooltipBeforeTimeout;
    let showingTooltipHandler;
    let handleMovingInsideOutsideTooltip;
    const timeToEnterTooltip = 500;
    const timeBeforeClosingTooltip = 500;
    const timeToStayOnTheLink = 500;
    const debugEvents = {};
    let open = false;
    let tooltipElement;
    bindLinkEnterEvent();

    function handleExitLink() {
        unbindLinkLeaveEvent();
        clearTimeout(showingTooltipHandler);
        if (open) {
            enterTooltipBeforeTimeout = setTimeout(() => {
                unbindTooltipEnterEvent();
                bindLinkEnterEvent();
                hide();
            }, timeBeforeClosingTooltip);
        } else {
            bindLinkEnterEvent();
        }
    }

    function updateTooltipElement(element) {
        tooltipElement = element;
    }

    function appearance() {
        show(updateTooltipElement);
        open = true;
        bindTooltipEnterEvent();
    }

    function enterLinkHandler() {
        unbindLinkEnterEvent();
        bindLinkLeaveEvent();
        showingTooltipHandler = setTimeout(appearance, timeToStayOnTheLink);
    }

    function handleEnterTooltip() {
        unbindTooltipEnterEvent();
        bindTooltipLeaveEvent();
        clearTimeout(handleMovingInsideOutsideTooltip);
        clearTimeout(enterTooltipBeforeTimeout);
    }

    function handleExitTooltip() {
        unbindTooltipLeaveEvent();
        bindTooltipEnterEvent();
        handleMovingInsideOutsideTooltip = setTimeout(() => {
            unbindTooltipEnterEvent();
            bindLinkEnterEvent();
            hide();
        }, timeToEnterTooltip);
    }

    function bindLinkEnterEvent() {
        debugEvents.enterLink += 1;
        linkElement.addEventListener('mouseover', enterLinkHandler);
    }

    function bindLinkLeaveEvent() {
        debugEvents.leaveLink += 1;
        linkElement.addEventListener('mouseleave', handleExitLink);
    }

    function unbindLinkEnterEvent() {
        debugEvents.enterLink -= 1;
        linkElement.removeEventListener('mouseover', enterLinkHandler);
    }

    function unbindLinkLeaveEvent() {
        debugEvents.leaveLink -= 1;
        linkElement.removeEventListener('mouseleave', handleExitLink);
    }

    function bindTooltipEnterEvent() {
        debugEvents.enterTooltip += 1;
        tooltipElement.addEventListener('mouseover', handleEnterTooltip);
    }

    function bindTooltipLeaveEvent() {
        debugEvents.leaveTooltip += 1;
        tooltipElement.addEventListener('mouseleave', handleExitTooltip);
    }

    function unbindTooltipEnterEvent() {
        debugEvents.enterTooltip -= 1;
        tooltipElement.removeEventListener('mouseover', handleEnterTooltip);
    }

    function unbindTooltipLeaveEvent() {
        debugEvents.leaveTooltip -= 1;
        tooltipElement.removeEventListener('mouseleave', handleExitTooltip);
    }
}