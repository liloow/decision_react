// @flow

import React from "react";
import classNames from "classnames";
import { PopoverPortal } from "./PopoverPortal";
import { getPosition, getStyles } from "./positionUtils";
import { eventsHandler } from "./eventsHandler";

type Props = {|
  children: React$Node,
  element: HTMLElement,
  getCursorPosition: Function
|};

type State = {|
  show: boolean,
  tooltipStyle: {},
  arrowStyle: {},
  arrowClass: string
|};

export class Popover extends React.Component<Props, State> {
  state = { show: false, tooltipStyle: {}, arrowStyle: {}, arrowClass: "" };

  componentDidMount() {
    eventsHandler({
      linkElement: this.props.element,
      show: this.show,
      hide: this.hide
    });
  }

  show = (cb: Function) => {
    const cursorPosition = this.props.getCursorPosition();
    const { tooltipYPosition, tooltipXPosition } = getPosition({
      window,
      cursorPosition
    });
    const { tooltipStyle, arrowStyle, arrowClass } = getStyles({
      tooltipYPosition,
      tooltipXPosition,
      window,
      cursorPosition
    });
    this.setState({ show: true, tooltipStyle, arrowStyle, arrowClass }, () =>
      cb(this.popoverRef)
    );
  };

  hide = () => {
    this.setState({ show: false });
  };

  popoverRef: ?HTMLDivElement;

  render() {
    const { show, tooltipStyle, arrowStyle, arrowClass } = this.state;
    return (
      show && (
        <PopoverPortal>
          <div
            style={{ ...arrowStyle }}
            className={classNames("popover-pane-arrow", arrowClass)}
          />
          <div
            ref={c => {
              this.popoverRef = c;
            }}
            className="popover-pane"
            style={{ display: "block", ...tooltipStyle }}
          >
            {this.props.children}
          </div>
        </PopoverPortal>
      )
    );
  }
}
