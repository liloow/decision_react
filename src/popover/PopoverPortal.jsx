  // @flow

  import React from 'react';
  import ReactDOM from 'react-dom';

  type Props = {|
    children: React$Node,
  |};

  export class PopoverPortal extends React.Component<Props> {
    constructor(props: Props) {
      super(props);
      this.element = document.createElement('div');
    }

    componentDidMount() {
      this.modalRoot = document.getElementById('popover-root');
      if (this.modalRoot) {
        this.modalRoot.appendChild(this.element);
      }
    }

    componentWillUnmount() {
      if (this.modalRoot) {
        this.modalRoot.removeChild(this.element);
      }
    }

    modalRoot: ?HTMLElement;
    element: HTMLElement;

    render() {
      return ReactDOM.createPortal(this.props.children, this.element);
    }
  }
