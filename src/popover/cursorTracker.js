// @flow

import type {
    CoordinatesType,
    CursorTrackerType
} from './types';

export const CursorTracker = {
    getInstance(): CursorTrackerType {
        let coordinates: CoordinatesType = {
            x: 0,
            y: 0
        };

        function onMouseMove(e: MouseEvent) {
            coordinates = {
                x: e.clientX,
                y: e.clientY
            };
        }
        window.addEventListener('mousemove', onMouseMove);

        return {
            getCursorPosition() {
                return coordinates;
            },
            removeListener() {
                window.removeEventListener('mousemove', onMouseMove);
            },
        };
    },
};