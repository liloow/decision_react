// @flow

import React from 'react';
import { reportRead } from './api';

type Props = {|
  readKey: string,
  entityType: 'lawyer' | 'lawFirm' | 'enterprise' | 'law' | 'jurisdiction' | "decision",
  children: React$Node
|};

export class ReportTimeSystem extends React.Component<Props> {
  componentDidMount() {
    window.addEventListener('focus', this.rtsActivity);
    window.addEventListener('scroll', this.rtsActivity);
    window.addEventListener('blur', this.rtsStopActivity);
    setInterval(this.rtsReportRead, this.REPORT_INTERVAL);
  }

  componentWillUnmount() {
    window.removeEventListener('focus', this.rtsActivity);
    window.removeEventListener('scroll', this.rtsActivity);
    window.removeEventListener('blur', this.rtsStopActivity);
  }

  REPORT_INTERVAL = 5000;
  INACTIVITY_DELAY = 20000;

  active = false;
  lastActivityStartedAt = 0;
  lastActivity = 0;
  lastReportedReadTime = 0;
  readTime = 0;

  rtsActivity = () => {
    if (!this.active) {
      this.active = true;
      this.lastActivityStartedAt = new Date().getTime();
    }
    this.lastActivity = new Date().getTime();
    const lastTime = { start: this.lastActivity };
    setTimeout(() => {
      if (this.lastActivity === lastTime.start) {
        this.rtsStopActivity();
      }
    }, this.INACTIVITY_DELAY);
  };

  rtsStopActivity = () => {
    if (this.active) {
      this.active = false;
      this.readTime =
        this.readTime + new Date().getTime() - this.lastActivityStartedAt;
      this.lastActivityStartedAt = 0;
    }
  };

  rtsGetReadTime = () => {
    let total = this.readTime;
    if (this.active) {
      const now = new Date().getTime();
      total += now - this.lastActivityStartedAt;
    }
    const readTime = Math.round(total / 1000);
    return readTime;
  };

  rtsReportRead = async () => {
    // const readTime = this.rtsGetReadTime();
    // if (!this.lastReportedReadTime || this.lastReportedReadTime < readTime) {
    //   this.lastReportedReadTime = readTime;
    //   try {
    //     await reportRead({
    //       key: this.props.readKey,
    //       readTime: this.readTime,
    //       entityType: this.props.entityType,
    //     });
    //   } catch (error) {
    //     // eslint-disable-next-line no-console
    //     console.log('Error in AJAX call of rtsReportRead');
    //   }
    // }
  };

  render() {
    return <div onMouseMove={this.rtsActivity}>{this.props.children}</div>;
  }
}
