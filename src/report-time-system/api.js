// @flow

import axios from 'axios';

// type Args = { |
//     key: string,
//     readTime: number,
//     entityType: 'lawyer' | 'lawFirm' | 'enterprise' | 'law' | 'jurisdiction',
//     |
// };

export function reportRead(args: Args) {
    return axios.post('/report_read', {
        key: args.key,
        readTime: args.readTime,
        type: args.entityType,
    });
}