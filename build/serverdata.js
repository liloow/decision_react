var visibleGhost = true
var decision_map = [];
var decision_map_data = {
    has_summary: false,
    has_chrono: true
};
var nav_hits = null;
var query_key = null;
var commentaires = [
    {
        format: "HTML",
        url:
            "http://mafr.fr/fr/article/cour-de-cassation-premiere-chambre-civile-11/",
        titre: "ARR\u00CAT DU 22 F\u00C9VRIER 1978, AFFAIRE DITE DU POUSSIN",
        date_html: "10 octobre 2010",
        auteur: "Marie-Anne Frison-Roche",
        nice_source: null,
        id: 391137
    },
    {
        format: "HTML",
        url:
            "http://www.dalloz-avocats.fr/documentation/Document?id=DZ/OASIS/000439",
        titre: "Erreur (Contrat)",
        date_html: null,
        auteur: null,
        nice_source: "Dalloz",
        id: 3753367
    },
    {
        format: "HTML",
        url:
            "http://actu.dalloz-etudiant.fr/a-la-une/article/contrat-de-vente-permis-de-construire-retroactivite-erreur/h/31f23c1c7429c1874418a77b3f26b834.html",
        titre:
            "Contrat de vente, permis de construire, r\u00E9troactivit\u00E9, erreur \u2026",
        date_html: "5 janvier 2017",
        auteur: "M. H.",
        nice_source: "Dalloz",
        id: 1578213
    }
];
var suivants_jp = null;
var arret_infos = {
    dateTexte: "19721213",
    doc_id: "DE8759599495785446",
    juridiction: "TGI",
    reference_url: "/d/TGI/Paris/1972/DE8759599495785446",
    comments: 3,
    links_decisions: 6,
    referer_host: null,
    action: "timeline"
};
var arret = {
    field_origin: {
        contenu_html: "TS",
        reference_url: "PERSO",
        lowest_id: "PERSO",
        lowest_created_at: "PERSO",
        id: "PERSO",
        doc_id: "PERSO",
        date_dec: "PERSO",
        juridiction: "PERSO",
        base: "PERSO",
        ville: "PERSO",
        wikipedia: "PERSO",
        created_at: "PERSO",
        updated_at: "PERSO",
        classified_at: "PERSO",
        parsed_at: "PERSO",
        dedup_at: "PERSO",
        dedup_info: "PERSO",
        linked_at: "PERSO",
        google: "PERSO",
        search_access: "PERSO",
        kw_generated_at: "PERSO",
        defendeurs_names: "PERSO"
    },
    field_origin_doc_id: {
        contenu_html: "DE8759599495785446_TS",
        reference_url: "DE8759599495785446",
        lowest_id: "DE8759599495785446",
        lowest_created_at: "DE8759599495785446",
        id: "DE8759599495785446",
        doc_id: "DE8759599495785446",
        date_dec: "DE8759599495785446",
        juridiction: "DE8759599495785446",
        base: "DE8759599495785446",
        ville: "DE8759599495785446",
        wikipedia: "DE8759599495785446",
        created_at: "DE8759599495785446",
        updated_at: "DE8759599495785446",
        classified_at: "DE8759599495785446",
        parsed_at: "DE8759599495785446",
        dedup_at: "DE8759599495785446",
        dedup_info: "DE8759599495785446",
        linked_at: "DE8759599495785446",
        google: "DE8759599495785446",
        search_access: "DE8759599495785446",
        kw_generated_at: "DE8759599495785446",
        defendeurs_names: "DE8759599495785446"
    },
    contenu_html:
        '<p>Attendu que les \u00E9poux Saint-Arroman ont assign\u00E9 le Direc\u00ADteur de la R\u00E9union des Mus\u00E9es nationaux, Rheims, ancien commissaire-priseur, Lebel, <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'expert\'></fiche-hover>" href="/?q=expert" class="link fichekeywordlink">expert</a>, et Laurin, commissaire-priseur, pour\u00A0: 1) entendre prononcer la nul\u00ADlit\u00E9 de la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'vente\'></fiche-hover>" href="/?q=vente" class="link fichekeywordlink">vente</a> aux ench\u00E8res publiques, faite au prix de 2\u202F200\u00A0F et \u00E0 la date du 21\u00A0f\u00E9vrier\u00A01968, d\u2019un tableau offert comme une \u0153uvre de l\u2019\u00E9cole des Carrache et dont l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'auteur\'></fiche-hover>" href="/?q=auteur" class="link fichekeywordlink">auteur</a> serait en r\u00E9alit\u00E9 Nicolas Poussin; 2) subsidiairement s\u2019entendre Rheims et Lebel condamner \u00E0 payer aux demandeurs une indemnit\u00E9 provisionnelle de 150\u202F000\u00A0F et entendre ordonner une <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'expertise\'></fiche-hover>" href="/?q=expertise" class="link fichekeywordlink">expertise</a> en vue d\u2019\u00E9valuer le <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'pr\u00E9judice\'></fiche-hover>" href="/?q=pr%C3%A9judice" class="link fichekeywordlink">pr\u00E9judice</a> subi par ceux-ci; </p><p> Attendu que les d\u00E9fendeurs concluent au <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'rejet\'></fiche-hover>" href="/?q=rejet" class="link fichekeywordlink">rejet</a> de ces demandes et sub- sidiairement, Rheims, Laurin et Lebel, \u00E0 une <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'expertise\'></fiche-hover>" href="/?q=expertise" class="link fichekeywordlink">expertise</a> technique; </p><p> Attendu que les \u00E9poux Saint-Arroman exposent que, d\u00E9sirant vendre le tableau litigieux, ils l\u2019ont pr\u00E9sent\u00E9 \u00E0 M<sup>e</sup>\u00A0Rheims; que celui-ci l\u2019a soumis \u00E0 l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'expert\'></fiche-hover>" href="/?q=expert" class="link fichekeywordlink">expert</a> Lebel; qu\u2019apr\u00E8s avis de ce dernier, la toile a \u00E9t\u00E9 inscrite au catalogue de l\u2019H\u00F4tel des <span class="decision_highlight">ventes de Paris comme se rattachant \u00E0 l\u2019Ecole des</span> Carrache; que Rheims a pr\u00E9venu les demandeurs qu\u2019elle pourrait &quot;faire en <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'vente\'></fiche-hover>" href="/?q=vente" class="link fichekeywordlink">vente</a> environ 1\u202F500\u00A0F&quot;; qu\u2019en fait, elle a \u00E9t\u00E9 adjug\u00E9e <span class="decision_highlight">pour 2\u202F200\u00A0F et que les Mus\u00E9es nationaux ont exerc\u00E9 leur <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'droit de pr\u00E9emption\'></fiche-hover>" href="/?q=droit%20de%20pr%C3%A9emption" class="link fichekeywordlink">droit</span> de pr\u00E9emption</a>\u202F; que la Revue du Louvre et des Mus\u00E9es de France, dans son num\u00E9ro 2, d\u00E8s 1969, annonce cette acquisition et affirme que l\u2019attribution \u00E0 Poussin est unanimement accept\u00E9e; que le tableau est expos\u00E9 au Louvre comme \u0153uvre de Poussin; qu\u2019ainsi les \u00E9poux Saint-Arroman ont, disent-ils, ali\u00E9n\u00E9 une toile de Poussin alors qu\u2019ils croyaient vendre un tableau de l\u2019\u00E9cole des Carrache, et par suite commis l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'erreur\'></fiche-hover>" href="/?q=erreur" class="link fichekeywordlink">erreur</a> sur la subs\u00ADtance qui rend nulle cette <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'vente\'></fiche-hover>" href="/?q=vente" class="link fichekeywordlink">vente</a>\u202F; que, dans le cas o\u00F9 la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'nullit\u00E9\'></fiche-hover>" href="/?q=nullit%C3%A9" class="link fichekeywordlink">nullit\u00E9</a> ne serait pas pronon\u00ADc\u00E9e, ils seraient alors fond\u00E9s \u00E0 demander au commissaire-priseur et \u00E0 l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'expert\'></fiche-hover>" href="/?q=expert" class="link fichekeywordlink">expert</a> la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'r\u00E9paration\'></fiche-hover>" href="/?q=r%C3%A9paration" class="link fichekeywordlink">r\u00E9paration</a> du <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'pr\u00E9judice\'></fiche-hover>" href="/?q=pr%C3%A9judice" class="link fichekeywordlink">pr\u00E9judice</a> qui leur a \u00E9t\u00E9 caus\u00E9 par les fautes professionnelles de ces derniers; </p><p> Attendu que la R\u00E9union des Mus\u00E9es nationaux, sans contester que l\u2019identit\u00E9 de l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'auteur\'></fiche-hover>" href="/?q=auteur" class="link fichekeywordlink">auteur</a> constitue une qualit\u00E9 substantielle de l\u2019\u0153uvre d\u2019art, soutient d\u2019une <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'part\'></fiche-hover>" href="/?q=part" class="link fichekeywordlink">part</a> que seul l\u2019acheteur pourrait faire <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'\u00E9tat\'></fiche-hover>" href="/?q=%C3%A9tat" class="link fichekeywordlink">\u00E9tat</a> de l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'erreur\'></fiche-hover>" href="/?q=erreur" class="link fichekeywordlink">erreur</a> commise \u00E0 ce point de vue, d\u2019autre <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'part\'></fiche-hover>" href="/?q=part" class="link fichekeywordlink">part</a> qu\u2019il r\u00E9sulterait des faits de la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'cause\'></fiche-hover>" href="/?q=cause" class="link fichekeywordlink">cause</a> et notamment des termes de l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'assignation\'></fiche-hover>" href="/?q=assignation" class="link fichekeywordlink">assignation</a> que les demandeurs n\u2019ont pas commis cette <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'erreur\'></fiche-hover>" href="/?q=erreur" class="link fichekeywordlink">erreur</a> alors qu\u2019avant la mise en <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'vente\'></fiche-hover>" href="/?q=vente" class="link fichekeywordlink">vente</a> ils consid\u00E9raient leur tableau comme &quot;un Poussin&quot;\u202F; </p><p> En droit :</p><p> Attendu, d\u2019une <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'part\'></fiche-hover>" href="/?q=part" class="link fichekeywordlink">part</a>, qu\u2019il est de principe, et qu\u2019il n\u2019est d\u2019ailleurs pas contest\u00E9 par la R\u00E9union des Mus\u00E9es Nationaux, que l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'erreur\'></fiche-hover>" href="/?q=erreur" class="link fichekeywordlink">erreur</a> sur la substance s\u2019entend, non seule\u00ADment de celle qui porte sur la mati\u00E8re dont la chose est compos\u00E9e; mais aussi de celle qui a trait aux <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'qualit\u00E9s\'></fiche-hover>" href="/?q=qualit%C3%A9s" class="link fichekeywordlink">qualit\u00E9s</a> substantielles d\u2019authenticit\u00E9 et d\u2019origine; </p><p> Attendu, d\u2019autre <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'part\'></fiche-hover>" href="/?q=part" class="link fichekeywordlink">part</a>, que, contrairement aux pr\u00E9tentions de la d\u00E9fenderesse, l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'erreur\'></fiche-hover>" href="/?q=erreur" class="link fichekeywordlink">erreur</a> sur la substance peut \u00EAtre all\u00E9gu\u00E9e aussi bien par le vendeur que par l\u2019acheteur, l\u2019article \n    <a data-doctrine-popover data-popover-template="<law-hover law=\'LEGIARTI000006436121\'></law-hover>" href="/?q=Article%201110%20du%20Code%20civil&law_article_id=LEGIARTI000006436121" class="link legilink">1110</a>\u00A0du Code civil ne faisant aucune distinction entre les contractants; qu\u2019en l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'esp\u00E8ce\'></fiche-hover>" href="/?q=esp%C3%A8ce" class="link fichekeywordlink">esp\u00E8ce</a>, par l\u2019exercice de son <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'droit de pr\u00E9emption\'></fiche-hover>" href="/?q=droit%20de%20pr%C3%A9emption" class="link fichekeywordlink">droit de pr\u00E9emption</a>, la d\u00E9fenderesse se trouve substitu\u00E9e \u00E0 l\u2019acheteur; </p><p> Attendu que par ailleurs, pour annuler l\u2019acte vici\u00E9 par l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'erreur\'></fiche-hover>" href="/?q=erreur" class="link fichekeywordlink">erreur</a> sur la substance, la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'jurisprudence\'></fiche-hover>" href="/?q=jurisprudence" class="link fichekeywordlink">jurisprudence</a> rel\u00E8ve comme \u00E9l\u00E9ment d\u00E9terminant de cette situation la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'comp\u00E9tence\'></fiche-hover>" href="/?q=comp%C3%A9tence" class="link fichekeywordlink">comp\u00E9tence</a> artistique ou technique du contractant <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'b\u00E9n\u00E9ficiaire\'></fiche-hover>" href="/?q=b%C3%A9n%C3%A9ficiaire" class="link fichekeywordlink">b\u00E9n\u00E9ficiaire</a> de cette <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'erreur\'></fiche-hover>" href="/?q=erreur" class="link fichekeywordlink">erreur</a>; </p><p> En fait :</p><p> Attendu que la R\u00E9union des Mus\u00E9es Nationaux maintient son opinion d\u00E9j\u00E0 proclam\u00E9e sur l\u2019attribution du tableau \u00E0 Poussin; </p><p> Attendu que cette opi\u00ADnion, exprim\u00E9e par une administration qui rassemble des experts particuli\u00E8rement \u00E9clair\u00E9s, doit \u00EAtre consid\u00E9r\u00E9e comme d\u00E9cisive, tout au moins dans ses rapports avec les demandeurs\u202F; </p><p> Attendu que par ailleurs cette haute <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'comp\u00E9tence\'></fiche-hover>" href="/?q=comp%C3%A9tence" class="link fichekeywordlink">comp\u00E9tence</a> fait appara\u00EEtre de fa\u00E7on \u00E9clatante l\u2019inf\u00E9riorit\u00E9 technique des vendeurs par rapport \u00E0 leur cocontrac- tant; </p><p> <span class="decision_highlight">Attendu certes que, pour tenter de d\u00E9montrer qu\u2019il n\u2019y avait pas eu <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'erreur\'></fiche-hover>" href="/?q=erreur" class="link fichekeywordlink">erreur</a> de la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'part\'></fiche-hover>" href="/?q=part" class="link fichekeywordlink">part</a> des vendeurs, la d\u00E9fenderesse fait <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'\u00E9tat\'></fiche-hover>" href="/?q=%C3%A9tat" class="link fichekeywordlink">\u00E9tat</a> des termes de l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'assignation\'></fiche-hover>" href="/?q=assignation" class="link fichekeywordlink">assignation</a> o\u00F9 il est \u00E9crit notamment\u00A0: &quot;Propri\u00E9taires d\u2019un tableau attribu\u00E9 \u00E0 Nicolas Poussin, ils ont d\u00E9cid\u00E9, en octobre 1967, de sa mise en <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'vente\'></fiche-hover>" href="/?q=vente" class="link fichekeywordlink">vente</a>\u2026&quot;, et plus loin\u00A0: &quot;l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'expertise\'></fiche-hover>" href="/?q=expertise" class="link fichekeywordlink">expertise</a> faite par M.\u00A0Lebel, <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'expert\'></fiche-hover>" href="/?q=expert" class="link fichekeywordlink">expert</a> en tableaux anciens, pr\u00E9cisait que le tableau n\u2019\u00E9tait pas du peintre Poussin mais de l\u2019\u00E9cole des Carrache</span>&quot;; </p><p> Mais <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'attendu que\'></fiche-hover>" href="/?q=attendu%20que" class="link fichekeywordlink">attendu que</a> pour <span class="decision_highlight">appr\u00E9cier si le <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'consentement\'></fiche-hover>" href="/?q=consentement" class="link fichekeywordlink">consentement</a> des vendeurs a \u00E9t\u00E9 vici\u00E9 par l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'erreur\'></fiche-hover>" href="/?q=erreur" class="link fichekeywordlink">erreur</a> sur la substance, c\u2019est \u00E0 leur opi\u00ADnion au</span> moment de la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'vente\'></fiche-hover>" href="/?q=vente" class="link fichekeywordlink">vente</a>, et \u00E0 elle seule, qu\u2019il convient de se r\u00E9f\u00E9rer; </p><p> qu\u2019\u00E0 ce moment ils s\u2019en sont enti\u00E8rement rapport\u00E9s \u00E0 la d\u00E9cision de l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'expert\'></fiche-hover>" href="/?q=expert" class="link fichekeywordlink">expert</a> en mettant en <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'vente\'></fiche-hover>" href="/?q=vente" class="link fichekeywordlink">vente</a> leur tableau comme \u00E9tant de l\u2019\u00E9cole des Carrache et au prix correspondant \u00E0 cette attribution; </p><p> que s\u2019ils avaient eu un motif s\u00E9rieux de penser que l\u2019\u0153uvre \u00E9tait un Poussin, ils n\u2019auraient pas ainsi accept\u00E9, sans recourir \u00E0 des recherches compl\u00E9mentaires, l\u2019avis et la mise \u00E0 prix du commissaire-priseur, et de l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'expert\'></fiche-hover>" href="/?q=expert" class="link fichekeywordlink">expert</a> que, par son <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'interm\u00E9diaire\'></fiche-hover>" href="/?q=interm%C3%A9diaire" class="link fichekeywordlink">interm\u00E9diaire</a>, ils avaient estim\u00E9 n\u00E9cessaire de consulter tant ils se sen\u00ADtaient incapables de d\u00E9terminer par eux-m\u00EAmes l\u2019origine de la toile litigieuse; </p><p> Attendu que, dans ces conditions, lors de la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'vente\'></fiche-hover>" href="/?q=vente" class="link fichekeywordlink">vente</a> de celle-ci, au prix de 2\u202F200\u00A0F, le 21\u00A0f\u00E9vrier\u00A01968, il n\u2019y a pas eu accord des contractants sur la chose vendue, les vendeurs croyant c\u00E9der un tableau de l\u2019\u00E9cole des Carrache, tandis que la R\u00E9union des mus\u00E9es nationaux estimait acqu\u00E9rir une \u0153uvre de Poussin; que la d\u00E9fenderesse a b\u00E9n\u00E9fici\u00E9 ainsi, gr\u00E2ce \u00E0 la grande sup\u00E9riorit\u00E9 de sa <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'comp\u00E9tence\'></fiche-hover>" href="/?q=comp%C3%A9tence" class="link fichekeywordlink">comp\u00E9tence</a> artistique, de l\u2019<a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'erreur\'></fiche-hover>" href="/?q=erreur" class="link fichekeywordlink">erreur</a> sur la substance commise par ses cocontractants, telle qu\u2019elle r\u00E9sultait des mentions port\u00E9es par eux sur le catalogue de l\u2019H\u00F4tel des ventes; que cette <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'erreur\'></fiche-hover>" href="/?q=erreur" class="link fichekeywordlink">erreur</a>, parfaitement connue de la d\u00E9fenderesse, a vici\u00E9 le <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'consentement\'></fiche-hover>" href="/?q=consentement" class="link fichekeywordlink">consentement</a> des vendeurs et que, par application de l\u2019article \n    <a data-doctrine-popover data-popover-template="<law-hover law=\'LEGIARTI000006436121\'></law-hover>" href="/?q=Article%201110%20du%20Code%20civil&law_article_id=LEGIARTI000006436121" class="link legilink">1110</a>\u00A0du Code civil, la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'vente\'></fiche-hover>" href="/?q=vente" class="link fichekeywordlink">vente</a> doit \u00EAtre d\u00E9clar\u00E9e nulle; </p><p> Attendu qu\u2019en cons\u00E9quence, Rheims, Lebel et Laurin doivent \u00EAtre mis <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'hors de cause\'></fiche-hover>" href="/?q=hors%20de%20cause" class="link fichekeywordlink">hors de cause</a>; </p><p> Attendu que, <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'faute\'></fiche-hover>" href="/?q=faute" class="link fichekeywordlink">faute</a> d\u2019urgence d\u00E9montr\u00E9e, il n\u2019y a pas lieu \u00E0 ex\u00E9\u00ADcution <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'provisoire\'></fiche-hover>" href="/?q=provisoire" class="link fichekeywordlink">provisoire</a>\u202F; </p><p> Par ces motifs :</p><p> Prononce la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'nullit\u00E9\'></fiche-hover>" href="/?q=nullit%C3%A9" class="link fichekeywordlink">nullit\u00E9</a> de la <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'vente\'></fiche-hover>" href="/?q=vente" class="link fichekeywordlink">vente</a>, intervenue le 21\u00A0f\u00E9vrier\u00A01968, du tableau appartenant aux demandeurs et acquis par la R\u00E9union des Mus\u00E9es nationaux; </p><p> Met <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'hors de cause\'></fiche-hover>" href="/?q=hors%20de%20cause" class="link fichekeywordlink">hors de cause</a> Rheims, Lebel et Laurin; </p><p> Dit n\u2019y avoir lieu \u00E0 <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'ex\u00E9cution provisoire\'></fiche-hover>" href="/?q=ex%C3%A9cution%20provisoire" class="link fichekeywordlink">ex\u00E9cution provisoire</a>\u202F; </p><p> Condamne la R\u00E9union des Mus\u00E9es nationaux aux <a data-doctrine-popover data-popover-template="<fiche-hover fiche=\'d\u00E9pens\'></fiche-hover>" href="/?q=d%C3%A9pens" class="link fichekeywordlink">d\u00E9pens</a>.</p>',
    bases: ["PERSO"],
    other_numbers: [],
    all_similar_doc_ids: ["DE8759599495785446"],
    reference_url: "/d/TGI/Paris/1972/DE8759599495785446",
    lowest_id: 17319134,
    lowest_created_at: "2017-05-05T19:53:26.531Z",
    id: 17319134,
    doc_id: "DE8759599495785446",
    date_dec: "1972-12-12T23:00:00.000Z",
    juridiction: "TGI",
    base: "PERSO",
    ville: "Paris",
    wikipedia: "Affaire du Poussin",
    created_at: "2017-05-05T19:53:26.531Z",
    updated_at: "2017-09-11T16:32:09.029Z",
    classified_at: "2017-05-06T00:33:28.125Z",
    parsed_at: "2017-09-11T22:42:16.164Z",
    dedup_at: "2018-01-03T12:45:13.660Z",
    dedup_info: { tested_duplicates: [] },
    linked_at: "2018-03-26T07:21:22.834Z",
    google: true,
    search_access: "ALL",
    kw_generated_at: "2017-05-07T05:00:42.411Z",
    defendeurs_names: [
        "Rheims",
        "Lebel",
        "Laurin",
        "R\u00E9union des Mus\u00E9es nationaux"
    ],
    loading_date: "2017-05-05",
    fixed_uppercase: false,
    sommaire_ana: null,
    parties_displayed:
        "Rheims, Lebel, Laurin, R\u00E9union des Mus\u00E9es nationaux",
    keywords: [
        "Poussin",
        "Mus\u00E9e",
        "Tableau",
        "Erreur",
        "Vente",
        "La r\u00E9union",
        "\u00C9cole",
        "Vendeur",
        "Expert",
        "Acheteur"
    ],
    arrets_dec_att: null,
    links_decisions: [
        {
            juridiction: "TCONFL",
            ville: null,
            doctype_cedh: null,
            doctype_branch: null,
            date_dec: "1975-06-01T23:00:00.000Z",
            doc_id: "CETATEXT000007604721",
            formation: null,
            base: "JADE",
            cjue_legal_type: null
        },
        {
            juridiction: "CA",
            ville: "Versailles",
            doctype_cedh: null,
            doctype_branch: null,
            date_dec: "1987-01-06T23:00:00.000Z",
            doc_id: "DE5557280089762529",
            formation: null,
            base: "PERSO",
            cjue_legal_type: null,
            solution_parsed: "Confirmation"
        },
        {
            juridiction: "CA",
            ville: "Paris",
            doctype_cedh: null,
            doctype_branch: null,
            date_dec: "1976-02-01T23:00:00.000Z",
            doc_id: "DE6363796161380821",
            formation: null,
            base: "PERSO",
            cjue_legal_type: null,
            solution_parsed: "Infirmation partielle"
        },
        {
            juridiction: "CA",
            ville: "Amiens",
            doctype_cedh: null,
            doctype_branch: null,
            date_dec: "1982-01-31T23:00:00.000Z",
            doc_id: "DE6783674836541966",
            formation: null,
            base: "PERSO",
            cjue_legal_type: null,
            solution_parsed: "Infirmation"
        },
        {
            juridiction: "CASS",
            ville: null,
            doctype_cedh: null,
            doctype_branch: null,
            date_dec: "1978-02-21T23:00:00.000Z",
            doc_id: "JURITEXT000007000241",
            formation: "Chambre civile 1",
            base: "CASS",
            cjue_legal_type: null,
            solution_parsed: "Cassation"
        },
        {
            juridiction: "CASS",
            ville: null,
            doctype_cedh: null,
            doctype_branch: null,
            date_dec: "1983-12-12T23:00:00.000Z",
            doc_id: "JURITEXT000007013245",
            formation: "Chambre civile 1",
            base: "CASS",
            cjue_legal_type: null,
            solution_parsed: "Cassation"
        }
    ],
    liens: null,
    short_title:
        "Affaire du Poussin, Tribunal de grande instance de Paris, 13 d\u00E9cembre 1972",
    juridiction_francais: "Tribunal de grande instance",
    juridiction_francais_avec_ville: "Tribunal de grande instance de Paris",
    reference_citation: "TGI Paris, 13 d\u00E9c. 1972",
    type_decision_cedh: null,
    type_decision_cc: null,
    president: "Boo; Poo",
    importance: 1,
    introduction_date: new Date(),
    separate_opinion: true
};
var backend_loading_times = { loading_time: 71, request_time: 76 };
var document_read_key = "336d38f0077f1d4b542ac78d012cbc0d";
var nb_textselection = 4;
var decision_clipboard =
    "TGI Paris, 13 d\u00E9c. 1972. Lire en ligne : https://www.doctrine.fr/d/TGI/Paris/1972/DE8759599495785446";
var decision_share = {
    text: "TGI Paris, 13 d\u00E9c. 1972",
    url: "https://www.doctrine.fr/d/TGI/Paris/1972/DE8759599495785446",
    description:
        "Attendu que les \u00E9poux Saint-Arroman ont assign\u00E9 le Direc\u00ADteur de la R\u00E9union des Mus\u00E9es nationaux, Rheims, ancien commissaire-priseur, Lebel, expert, et Laurin, commissaire-priseur, pour : 1) entendre prononcer la nul\u00ADlit\u00E9 de la vente aux ench\u00E8res\u2026"
};

var backend_loading_times = { loading_time: 76, request_time: 77 };
var document_read_key = "04dd588d380a4a4d9c3d5c8eb3f3d3c1";
var nb_textselection = 4;
var decision_clipboard =
    "TGI Paris, 13 d\u00E9c. 1972. Lire en ligne : https://www.doctrine.fr/d/TGI/Paris/1972/DE8759599495785446";
var decision_share = {
    text: "TGI Paris, 13 d\u00E9c. 1972",
    url: "https://www.doctrine.fr/d/TGI/Paris/1972/DE8759599495785446",
    description:
        "Attendu que les \u00E9poux Saint-Arroman ont assign\u00E9 le Direc\u00ADteur de la R\u00E9union des Mus\u00E9es nationaux, Rheims, ancien commissaire-priseur, Lebel, expert, et Laurin, commissaire-priseur, pour : 1) entendre prononcer la nul\u00ADlit\u00E9 de la vente aux ench\u00E8res\u2026"
};
var datasetarray = [
    {
        id: 1,
        content:
            '<a class="decision__timeline__item pointer_cursor link" target="_self" href="/d/TCONFL/1975/CETATEXT000007604721?action=timeline" >TCONFL</a>',
        title: "<span>Lire la d\u00E9cision</span>",
        start: "1975-06-01T23:00:00.000Z"
    },
    {
        id: 2,
        content:
            '<a class="decision__timeline__item pointer_cursor link" target="_self" href="/d/CA/Versailles/1987/DE5557280089762529?action=timeline" >CA Versailles<br><small>Confirmation</small></a>',
        title: "<span>Lire la d\u00E9cision</span>",
        start: "1987-01-06T23:00:00.000Z"
    },
    {
        id: 3,
        content:
            '<a class="decision__timeline__item pointer_cursor link" target="_self" href="/d/CA/Paris/1976/DE6363796161380821?action=timeline" >CA Paris<br><small>Infirmation partielle</small></a>',
        title: "<span>Lire la d\u00E9cision</span>",
        start: "1976-02-01T23:00:00.000Z"
    },
    {
        id: 4,
        content:
            '<a class="decision__timeline__item pointer_cursor link" target="_self" href="/d/CA/Amiens/1982/DE6783674836541966?action=timeline" >CA Amiens<br><small>Infirmation</small></a>',
        title: "<span>Lire la d\u00E9cision</span>",
        start: "1982-01-31T23:00:00.000Z"
    },
    {
        id: 5,
        content:
            '<a class="decision__timeline__item pointer_cursor link" target="_self" href="/d/CASS/1978/JURITEXT000007000241?action=timeline" >CASS<br><small>Cassation</small></a>',
        title: "<span>Lire la d\u00E9cision</span>",
        start: "1978-02-21T23:00:00.000Z"
    },
    {
        id: 6,
        content:
            '<a class="decision__timeline__item pointer_cursor link" target="_self" href="/d/CASS/1983/JURITEXT000007013245?action=timeline" >CASS<br><small>Cassation</small></a>',
        title: "<span>Lire la d\u00E9cision</span>",
        start: "1983-12-12T23:00:00.000Z"
    },
    {
        id: 7,
        content: '<span class="decision__timeline__item" >TGI Paris</span>',
        title:
            '<span class="vis-tooltip">D\u00E9cision en cours de lecture</span>',
        start: "1972-12-12T23:00:00.000Z",
        className: "decision__timeline__item--current"
    }
];
var links_decisions = [
    {
        juridiction: "TCONFL",
        ville: null,
        doctype_cedh: null,
        doctype_branch: null,
        date_dec: "1975-06-01T23:00:00.000Z",
        doc_id: "CETATEXT000007604721",
        formation: null,
        base: "JADE",
        cjue_legal_type: null
    },
    {
        juridiction: "CA",
        ville: "Versailles",
        doctype_cedh: null,
        doctype_branch: null,
        date_dec: "1987-01-06T23:00:00.000Z",
        doc_id: "DE5557280089762529",
        formation: null,
        base: "PERSO",
        cjue_legal_type: null,
        solution_parsed: "Confirmation"
    },
    {
        juridiction: "CA",
        ville: "Paris",
        doctype_cedh: null,
        doctype_branch: null,
        date_dec: "1976-02-01T23:00:00.000Z",
        doc_id: "DE6363796161380821",
        formation: null,
        base: "PERSO",
        cjue_legal_type: null,
        solution_parsed: "Infirmation partielle"
    },
    {
        juridiction: "CA",
        ville: "Amiens",
        doctype_cedh: null,
        doctype_branch: null,
        date_dec: "1982-01-31T23:00:00.000Z",
        doc_id: "DE6783674836541966",
        formation: null,
        base: "PERSO",
        cjue_legal_type: null,
        solution_parsed: "Infirmation"
    },
    {
        juridiction: "CASS",
        ville: null,
        doctype_cedh: null,
        doctype_branch: null,
        date_dec: "1978-02-21T23:00:00.000Z",
        doc_id: "JURITEXT000007000241",
        formation: "Chambre civile 1",
        base: "CASS",
        cjue_legal_type: null,
        solution_parsed: "Cassation"
    },
    {
        juridiction: "CASS",
        ville: null,
        doctype_cedh: null,
        doctype_branch: null,
        date_dec: "1983-12-12T23:00:00.000Z",
        doc_id: "JURITEXT000007013245",
        formation: "Chambre civile 1",
        base: "CASS",
        cjue_legal_type: null,
        solution_parsed: "Cassation"
    }
];
var decision_comments = [
    {
        format: "HTML",
        url:
            "http://mafr.fr/fr/article/cour-de-cassation-premiere-chambre-civile-11/",
        titre: "ARR\u00CAT DU 22 F\u00C9VRIER 1978, AFFAIRE DITE DU POUSSIN",
        date_html: "10 octobre 2010",
        auteur: "Marie-Anne Frison-Roche",
        nice_source: null,
        id: 391137
    },
    {
        format: "HTML",
        url:
            "http://www.dalloz-avocats.fr/documentation/Document?id=DZ/OASIS/000439",
        titre: "Erreur (Contrat)",
        date_html: null,
        auteur: null,
        nice_source: "Dalloz",
        id: 3753367
    },
    {
        format: "HTML",
        url:
            "http://actu.dalloz-etudiant.fr/a-la-une/article/contrat-de-vente-permis-de-construire-retroactivite-erreur/h/31f23c1c7429c1874418a77b3f26b834.html",
        titre:
            "Contrat de vente, permis de construire, r\u00E9troactivit\u00E9, erreur \u2026",
        date_html: "5 janvier 2017",
        auteur: "M. H.",
        nice_source: "Dalloz",
        id: 1578213
    }
];
var fakeMorelikethis = {
    high_hits: [
        {
            doc_id: "CJUE62001CJ0388",
            reference_url: "/d/CJUE/2003/CJUE62001CJ0388",
            titre:
                "CJCE, n° C-388&#x2F;01, Arrêt de la Cour, Commission des Communautés européennes contre République italienne, 16 janvier 2003, Commission c&#x2F; Italie",
            raw_keywords: [
                "Avantages tarifaires accordés par les collectivités locales ou nationales décentralisées réservés aux ressortissants nationaux ou aux résidents sur le territoire desdites collectivités qui sont âgés de 60 ou 65 ans",
                "Accès aux musées, monuments, galeries, fouilles archéologiques, parcs et jardins classés monuments publics",
                "Cee/ce - libre circulation des personnes et des services * libre circulation des personnes et des services",
                "Cee/ce - les principes et la citoyenneté de l'union * les principes et la citoyenneté de l'union",
                "Interdiction de discrimination en raison de la nationalité",
                "Non-discrimination en raison de la nationalité",
                "Principes, objectifs et missions des traités",
                "Discrimination en raison de la nationalité",
                "Libre prestation des services",
                "Absence rt. 12 ce et 49 ce)"
            ],
            html_keywords: [
                "Avantages tarifaires accordés par les collectivités locales ou nationales décentralisées réservés aux ressortissants nationaux ou aux résidents sur le territoire desdites collectivités qui sont âgés de 60 ou 65 ans",
                "Accès aux musées, monuments, galeries, fouilles archéologiques, parcs et jardins classés monuments publics",
                "Cee&#x2F;ce - libre circulation des personnes et des services * libre circulation des personnes et des services",
                "Cee&#x2F;ce - les principes et la citoyenneté de l&#x27;union * les principes et la citoyenneté de l&#x27;union",
                "Interdiction de discrimination en raison de la nationalité",
                "Non-discrimination en raison de la nationalité",
                "Principes, objectifs et missions des traités",
                "Discrimination en raison de la nationalité",
                "Libre prestation des services",
                "Absence rt. 12 ce et 49 ce)"
            ],
            solution: null,
            arret_dec_att: null,
            sommaire_ana:
                "Avis juridique important | 62001J0388 Arrêt de la Cour (sixième chambre) du 16 janvier 2003. – Commission des Communautés européennes contre République italienne. – Manquement d’État – Libre prestation des services – Non-discrimination – Articles 12 CE et 49 CE – Accès aux musées, monuments, galeries, fouilles archéologiques, parcs et jardins classés monuments publics – Tarifs préférentiels accordés par les collectivités locales ou nationales décentralisées. – Affaire C-388/01. Recueil de jurisprudence 2003 page I-00721 Sommaire Parties Motifs de l’arrêt …",
            juridiction: "CJUE",
            automatic_metadata: {},
            number_of_comments: 6,
            comments: [
                {
                    titre:
                        "Dossier documentaire de la décision n° 2015-520 QPC du 3 février 2015, Société Metro Holding France SA venant aux droits de la société CRFP Cash [Application du régime fiscal des sociétés mères aux produits de titres auxquels ne sont pas attachés des droits de vote]",
                    author: "Conseil constitutionnel",
                    url: "/d/CJUE62001CJ0388?redirect_to_comment=16051"
                },
                {
                    titre:
                        "Les résidents de l'Union européenne sont soumis au même tarif pour l'accès aux musées",
                    author: "La Rédaction",
                    url: "/d/CJUE62001CJ0388?redirect_to_comment=1545686"
                },
                {
                    titre:
                        "Tarif préférentiel pour la location d'une salle des fêtes",
                    author: "Delphine Fenasse",
                    url: "/d/CJUE62001CJ0388?redirect_to_comment=2166184"
                },
                {
                    titre:
                        "Vide-grenier : tarifs d’occupation du domaine public liés à la résidence des personnes",
                    author: "Melissa Pinto",
                    url: "/d/CJUE62001CJ0388?redirect_to_comment=2187566"
                },
                {
                    titre:
                        "Gratuité dans les musées : le principe d’égalité appliqué aux personnes en situation irrégulière",
                    author: "Philippe Cossalter",
                    url: "/d/CJUE62001CJ0388?redirect_to_comment=3185781"
                },
                {
                    titre: "Microsoft Word - CP030004FR.doc",
                    author: "Curia",
                    url: "/d/CJUE62001CJ0388?redirect_to_comment=554295360"
                }
            ],
            reference_citation:
                "CJUE, Arrêt du 16 janvier 2003, Commission / Italie, C-388/01, EU:C:2003:30",
            visited: false,
            in_folder: false,
            in_folders: [],
            validation: null
        },
        {
            doc_id: "CEW:FR:CESSR:2002:239064.20021113",
            reference_url: "/d/CE/2002/CEW:FR:CESSR:2002:239064.20021113",
            titre:
                "Conseil d&#x27;Etat, 7 &#x2F; 5 SSR, du 13 novembre 2002, 239064, publié au recueil Lebon",
            raw_keywords: [
                "Accès à l'emploi rendu possible aux fonctionnaires pouvant accéder au corps des conservateurs généraux du patrimoine par la voie du détachement",
                "Elargissement de la catégorie de fonctionnaires à l'intérieur de laquelle le directeur peut être nommé",
                "Creation, transformation ou suppression de corps, de cadres d'emplois, grades et emplois",
                "Elargissement des critères requis pour être nommé dans l'emploi concerné",
                "A) décret portant statut de l'emploi de directeur du musée d'orsay",
                "B) décret portant statut de l'emploi de directeur du musée d'orsay",
                "B) décret portant nomination du directeur du musée d'orsay",
                "C) décret portant nomination du directeur du musée d'orsay",
                "Appréciations soumises a un contrôle restreint",
                "Contrôle sur l'ampleur de l'élargissement"
            ],
            html_keywords: [
                "Accès à l&#x27;emploi rendu possible aux fonctionnaires pouvant accéder au corps des conservateurs généraux du patrimoine par la voie du détachement",
                "Elargissement de la catégorie de fonctionnaires à l&#x27;intérieur de laquelle le directeur peut être nommé",
                "Creation, transformation ou suppression de corps, de cadres d&#x27;emplois, grades et emplois",
                "Elargissement des critères requis pour être nommé dans l&#x27;emploi concerné",
                "A) décret portant statut de l&#x27;emploi de directeur du musée d&#x27;orsay",
                "B) décret portant statut de l&#x27;emploi de directeur du musée d&#x27;orsay",
                "B) décret portant nomination du directeur du musée d&#x27;orsay",
                "C) décret portant nomination du directeur du musée d&#x27;orsay",
                "Appréciations soumises a un contrôle restreint",
                "Contrôle sur l&#x27;ampleur de l&#x27;élargissement"
            ],
            solution: "Réformation",
            arret_dec_att: null,
            sommaire_ana:
                "a) Le décret portant statut de l’emploi de directeur du musée d’Orsay prévoit que ledit directeur peut être choisi, non seulement parmi les conservateurs généraux et conservateurs en chef du patrimoine, mais aussi parmi les fonctionnaires pouvant accéder au corps des conservateurs généraux du patrimoine par la voie du détachement. En procédant à cet élargissement des conditions requises pour être nommé à l’emploi de directeur du musée d’Orsay, le pouvoir réglementaire n’a pas commis d’erreur manifeste d’appréciation. b) Le juge de l’excès de pouvoir exerce un contrôle restreint sur le…",
            juridiction: "CE",
            automatic_metadata: {},
            number_of_comments: 3,
            comments: [
                {
                    titre:
                        "A propos des statuts d'emplois : il est possible de modifier un statut d'emploi pour permettre la nomination de quelqu'un qui ne satisfait pas aux conditions de nomination que ce statut pose",
                    author: "Céline Cros",
                    url:
                        "/d/CEW:FR:CESSR:2002:239064.20021113?redirect_to_comment=1545549"
                },
                {
                    titre:
                        "Conclusions du rapporteur public sur l'affaire n°401796",
                    author: "Conseil d'État",
                    url:
                        "/d/CEW:FR:CESSR:2002:239064.20021113?redirect_to_comment=711582644"
                },
                {
                    titre:
                        "Conclusions du rapporteur public sur l'affaire n°410757",
                    author: "Conseil d'État",
                    url:
                        "/d/CEW:FR:CESSR:2002:239064.20021113?redirect_to_comment=869867772"
                }
            ],
            reference_citation:
                "CE, 7 / 5 ss-sect. réunies, 13 nov. 2002, n° 239064, Lebon",
            visited: false,
            in_folder: false,
            in_folders: [],
            validation: null
        },
        {
            doc_id: "CEDH001-85075",
            reference_url: "/d/CEDH/HFJUD/CHAMBER/2008/CEDH001-85075",
            titre:
                "CEDH, Cour (cinquième section), AFFAIRE GLASER c. REPUBLIQUE TCHEQUE, 14 février 2008, 55179&#x2F;00",
            raw_keywords: [
                "Musée",
                "Juif",
                "Collection",
                "République tchèque",
                "Gouvernement",
                "Cour constitutionnelle",
                "Protocole",
                "Objet d'art",
                "Droit de propriété",
                "Action en revendication"
            ],
            html_keywords: [
                "Musée",
                "Juif",
                "Collection",
                "République tchèque",
                "Gouvernement",
                "Cour constitutionnelle",
                "Protocole",
                "Objet d&#x27;art",
                "Droit de propriété",
                "Action en revendication"
            ],
            solution: null,
            arret_dec_att: null,
            sommaire_ana:
                "CINQUIÈME SECTION AFFAIRE GLASER c. RÉPUBLIQUE TCHÈQUE (Requête no 55179/00) ARRÊT STRASBOURG 14 février 2008 DÉFINITIF 14/05/2008 Cet arrêt deviendra définitif dans les conditions définies à l’article 44 § 2 de la Convention. Il peut subir des retouches de forme. En l’affaire Glaser c. République tchèque, La Cour européenne des droits de l’homme (cinquième section), siégeant en une chambre composée de : Peer Lorenzen, président, Snejana Botoucharova, Karel Jungwiert, Volodymyr Butkevych, Margarita Tsatsa-Nikolovska, Rait Maruste, Mark Villiger, juges, et de…",
            juridiction: "CEDH",
            automatic_metadata: {},
            number_of_comments: 3,
            comments: [
                {
                    titre:
                        "Communiqué de presse sur les affaires 5950/05, 17294/04, 14385/04, 34151/04, 54476/00, 9323/03, 502/03, 19646/03, 25949/03, 25976/03, …",
                    author: "CEDH",
                    url: "/d/CEDH001-85075?redirect_to_comment=254252"
                },
                {
                    titre:
                        "Communiqué de presse sur les affaires 13324/04, 30810/03, 55179/00, 40067/06, 1526/02, 1528/02, 67007/01, 66802/01, 4537/04, 14019/05, …",
                    author: "CEDH",
                    url: "/d/CEDH001-85075?redirect_to_comment=254263"
                },
                {
                    titre: "Note d’information sur l'affaire 55179/00",
                    author: "CEDH",
                    url: "/d/CEDH001-85075?redirect_to_comment=254272"
                }
            ],
            reference_citation:
                "CEDH, Cour (Cinquième Section), 14 févr. 2008, n° 55179/00",
            visited: false,
            in_folder: false,
            in_folders: [],
            validation: null
        },
        {
            doc_id: "CETATEXT000007760427",
            reference_url: "/d/CE/1990/CETATEXT000007760427",
            titre:
                "Conseil d&#x27;Etat, Section, du 30 novembre 1990, 100812 100941, publié au recueil Lebon, Ville d&#x27;Orléans et Ministre de la culture, de la communication, des grands…",
            raw_keywords: [
                "Retrait de la demande de licence d'exportation après qu'il a été statué sur cette demande",
                "Rétention d'œuvres d'art proposées à l'exportation au profit des collections publiques",
                "Circonstances n'entachant pas d'illégallité la décision prise",
                "Généralités -régime des œuvres d'art",
                "Arts et lettres",
                "Conditions",
                "Musée",
                "Tribunaux administratifs",
                "Culture",
                "Grands travaux"
            ],
            html_keywords: [
                "Retrait de la demande de licence d&#x27;exportation après qu&#x27;il a été statué sur cette demande",
                "Rétention d&#x27;œuvres d&#x27;art proposées à l&#x27;exportation au profit des collections publiques",
                "Circonstances n&#x27;entachant pas d&#x27;illégallité la décision prise",
                "Généralités -régime des œuvres d&#x27;art",
                "Arts et lettres",
                "Conditions",
                "Musée",
                "Tribunaux administratifs",
                "Culture",
                "Grands travaux"
            ],
            solution: "Annulation",
            arret_dec_att: ["Tribunal administratif de Paris, 8 juin 1988"],
            sommaire_ana:
                "Aux termes de l’article 1er de la loi du 23 juin 1941 relative à l’exportation des oeuvres d’art : &quot;Les objets présentant un intérêt national d’histoire ou d’art ne pourront être exportés sans une autorisation du secrétaire d’Etat à l’éducation nationale et à la jeunesse, qui devra se prononcer dans le délai d’un mois à partir de la déclaration fournie à la douane par l’exportateur ()&quot; et aux termes de l’article 2 de la même loi : &quot;l’Etat a le droit de retenir () au prix fixé par l’exportateur, les objets proposés à l’exportation. Ce droit pourra s’exercer pendant une période de six mois&quot;. Par…",
            juridiction: "CE",
            automatic_metadata: {},
            number_of_comments: 2,
            comments: [
                {
                    titre:
                        "Conclusions du rapporteur public sur l'affaire n°387322",
                    author: "Conseil d'État",
                    url: "/d/CETATEXT000007760427?redirect_to_comment=6161634"
                },
                {
                    titre:
                        "Conclusions du rapporteur public sur l'affaire n°398574",
                    author: "Conseil d'État",
                    url: "/d/CETATEXT000007760427?redirect_to_comment=6161914"
                }
            ],
            reference_citation:
                "CE, sect., 30 nov. 1990, n° 100812 100941, Lebon",
            visited: false,
            in_folder: false,
            in_folders: [],
            validation: null
        },
        {
            doc_id: "CEW:FR:CESSR:1996:163528.19960228",
            reference_url: "/d/CE/1996/CEW:FR:CESSR:1996:163528.19960228",
            titre:
                "Conseil d&#x27;Etat, 10 &#x2F; 7 SSR, du 28 février 1996, 163528, mentionné aux tables du recueil Lebon, Etablissement public du musée du Louvre",
            raw_keywords: [
                "Égalité des usagers devant le service public -fixation de tarifs différents pour un même service rendu",
                "Droits de réservation obligatoires du musée du louvre pour les groupes de visiteurs",
                "Redevances -fixation de tarifs différents pour un même service rendu",
                "Droits de réservation obligatoires pour les groupes de visiteurs",
                "Fixation de tarifs différents pour un même service rendu",
                "Parafiscalite, redevances et taxes diverses",
                "Discrimination injustifiée en l'espèce",
                "Violation directe de la règle de droit",
                "Actes législatifs et administratifs",
                "Validité des actes administratifs"
            ],
            html_keywords: [
                "Égalité des usagers devant le service public -fixation de tarifs différents pour un même service rendu",
                "Droits de réservation obligatoires du musée du louvre pour les groupes de visiteurs",
                "Redevances -fixation de tarifs différents pour un même service rendu",
                "Droits de réservation obligatoires pour les groupes de visiteurs",
                "Fixation de tarifs différents pour un même service rendu",
                "Parafiscalite, redevances et taxes diverses",
                "Discrimination injustifiée en l&#x27;espèce",
                "Violation directe de la règle de droit",
                "Actes législatifs et administratifs",
                "Validité des actes administratifs"
            ],
            solution: "Rejet",
            arret_dec_att: ["Tribunal administratif de Paris, 8 mars 1994"],
            sommaire_ana:
                "La fixation de tarifs différents applicables, pour un même service rendu, à diverses catégories d’usagers d’un service public implique, à moins qu’elle ne soit la conséquence nécessaire d’une loi, qu’il existe entre les usagers des différences de situation appréciables ou que cette mesure soit justifiée par une nécessité d’intérêt général en rapport avec les conditions d’exploitation du service. Délibération du conseil d’administration du musée du Louvre ayant instauré des droits de réservation obligatoires pour les groupes dits &quot;libres&quot; et décidé l’exonération d’un tel droit de réservation…",
            juridiction: "CE",
            automatic_metadata: {},
            number_of_comments: 1,
            comments: [
                {
                    titre:
                        "Détermination du montant de la redevance pour service rendu",
                    author: "Julien Martin",
                    url:
                        "/d/CEW:FR:CESSR:1996:163528.19960228?redirect_to_comment=3187488"
                }
            ],
            reference_citation:
                "CE, 10 / 7 ss-sect. réunies, 28 févr. 1996, n° 163528, Lebon T.",
            visited: false,
            in_folder: false,
            in_folders: [],
            validation: null
        }
    ],
    low_hits: [
        {
            doc_id: "FRC38689722886CFDF4F35",
            reference_url: "/d/TGI/Paris/2010/FRC38689722886CFDF4F35",
            titre:
                "Tribunal de grande instance de Paris, Juge de l&#x27;exécution, 6 août 2010, n° 10&#x2F;81832",
            raw_keywords: [
                "Musée",
                "Papier",
                "Mise à jour",
                "Veuve",
                "Catalogue",
                "Oeuvre",
                "Astreinte",
                "Ouvrage",
                "Publication",
                "Exécution"
            ],
            html_keywords: [
                "Musée",
                "Papier",
                "Mise à jour",
                "Veuve",
                "Catalogue",
                "Oeuvre",
                "Astreinte",
                "Ouvrage",
                "Publication",
                "Exécution"
            ],
            solution: null,
            arret_dec_att: null,
            sommaire_ana:
                "T R I B U N A L D E GRANDE I N S T A N C E D E P A R I S ■ N° RG : 10/81832 N° MINUTE : copies exécutoires envoyées par LRAR aux parties et expéditions envoyées aux parties et aux avocats le SERVICE DU JUGE DE L’EXÉCUTION JUGEMENT rendu le 06 août 2010 DEMANDEURS Madame Z A veuve X 195 route Saint-Antoine [] Monsieur B X [] [] représentés tous deux par Me I-Luc MATHON, avocat au barreau de PARIS, vestiaire : #A0458 DÉFENDEURS Madame C D [] [] Monsieur E F dit Y [] [] représentés tous deux par Me Françoise SALLIOU, avocat au…",
            juridiction: "TGI",
            automatic_metadata: {},
            number_of_comments: 0,
            reference_citation: "TGI Paris, JEX, 6 août 2010, n° 10/81832",
            visited: false,
            in_folder: false,
            in_folders: [],
            validation: {
                juridiction: "Cour d'appel",
                of_juridiction: "de la Cour d'appel",
                solution: "Confirmation",
                validated: true,
                url:
                    "/d/CA/Paris/2011/R56360ADC32FD057B4870?origin=FRC38689722886CFDF4F35&action=search_arret_validation&event_key=2018-05-31-4257430"
            }
        },
        {
            doc_id: "A69E2E8FA6A37AC8A45AB",
            reference_url: "/d/TA/Rouen/2014/A69E2E8FA6A37AC8A45AB",
            titre:
                "Tribunal administratif de Rouen, 4 novembre 2014, n° 1301906",
            raw_keywords: [
                "Musée",
                "Ingénieur",
                "Documentation",
                "Directeur général",
                "Recherche",
                "Tribunaux administratifs",
                "Education",
                "Positionnement",
                "Mission",
                "Statut"
            ],
            html_keywords: [
                "Musée",
                "Ingénieur",
                "Documentation",
                "Directeur général",
                "Recherche",
                "Tribunaux administratifs",
                "Education",
                "Positionnement",
                "Mission",
                "Statut"
            ],
            solution: "Rejet",
            arret_dec_att: null,
            sommaire_ana:
                "TRIBUNAL ADMINISTRATIF DE ROUEN N°1301906 ___________ M. A Y ___________ Mme X Rapporteur ___________ M. Bertoncini Rapporteur public ___________ Audience du 14 octobre 2014 Lecture du 4 novembre 2014 ___________ jr RÉPUBLIQUE FRANÇAISE AU NOM DU PEUPLE FRANÇAIS Le tribunal administratif de Rouen (1re Chambre) PCJA : 36-05-01 Code de publication : C Vu le jugement, en date du 3 juillet 2013, enregistré au greffe du tribunal administratif de Poitiers le 10 septembre 2012 sous le n°1202241, par lequel le magistrat désigné a transmis au tribunal administratif de Rouen, en…",
            juridiction: "TA",
            automatic_metadata: {},
            number_of_comments: 0,
            reference_citation: "TA Rouen, 4 nov. 2014, n° 1301906",
            visited: false,
            in_folder: false,
            in_folders: [],
            validation: null
        },
        {
            doc_id: "A2B3A0CAEDFE352C92C82",
            reference_url: "/d/TA/Versailles/2010/A2B3A0CAEDFE352C92C82",
            titre:
                "Tribunal administratif de Versailles, 10 juin 2010, n° 0710233",
            raw_keywords: [
                "Musée",
                "Etablissement public",
                "Enfant",
                "Justice administrative",
                "Mobilier",
                "Préjudice",
                "Voiture",
                "Responsabilité",
                "Véhicule",
                "Restitution"
            ],
            html_keywords: [
                "Musée",
                "Etablissement public",
                "Enfant",
                "Justice administrative",
                "Mobilier",
                "Préjudice",
                "Voiture",
                "Responsabilité",
                "Véhicule",
                "Restitution"
            ],
            solution: "Rejet",
            arret_dec_att: null,
            sommaire_ana:
                "TRIBUNAL ADMINISTRATIF DE VERSAILLES N° 0710233 ___________ Mme Z Y et autres ___________ M. Lombard Rapporteur ___________ Mme Viseur-Ferré Rapporteur public ___________ Audience du 27 mai 2010 Lecture du 10 juin 2010 ___________ vf RÉPUBLIQUE FRANÇAISE AU NOM DU PEUPLE FRANÇAIS Le Tribunal administratif de Versailles (1re chambre) Vu la requête, enregistrée le 11 octobre 2007, présentée pour Mme Z Y et ses deux enfants mineurs, Dimitri et Élisa Y, demeurant à XXX, par Me Geuzimian ; Mme Y et ses enfants demandent au Tribunal : 1°) de condamner l’établissement public…",
            juridiction: "TA",
            automatic_metadata: {},
            number_of_comments: 0,
            reference_citation: "TA Versailles, 10 juin 2010, n° 0710233",
            visited: false,
            in_folder: false,
            in_folders: [],
            validation: null
        },
        {
            doc_id: "DE5557280089762529",
            reference_url: "/d/CA/Versailles/1987/DE5557280089762529",
            titre:
                "Affaire du Poussin, Cour d&#x27;appel de Versailles, 7 janvier 1987, Réunion des Musées nationaux",
            raw_keywords: [
                "Poussin",
                "Tableau",
                "Erreur",
                "Musée",
                "Attribution",
                "Vente",
                "École",
                "Tradition",
                "Expert",
                "Culture"
            ],
            html_keywords: [
                "Poussin",
                "Tableau",
                "Erreur",
                "Musée",
                "Attribution",
                "Vente",
                "École",
                "Tradition",
                "Expert",
                "Culture"
            ],
            solution: "Confirmation",
            arret_dec_att: null,
            sommaire_ana:
                "Considérant qu’il est constant et non dénié que les époux Saint-Arroman, propriétaires d’un tableau, ont décidé en 1968 de s’en séparer, ayant besoin d’argent à la suite de la mutation professionnelle du mari de province à Paris; qu’ils sont allés trouver Me Rheims, commissaire-priseur, pour lui confier la vente aux enchères publiques de ce tableau; que M. Lebel, expert de l’officier public, a conclu qu’il s’agissait d’une œuvre anonyme de l’Ecole des Carrache qui représentait une valeur de 1 500 F; que le tableau, mis en vente sous cette attribu­tion, a été adjugé au prix de 2 200 F le 21…",
            juridiction: "CA",
            automatic_metadata: {},
            number_of_comments: 0,
            reference_citation: "CA Versailles, 7 janv. 1987",
            visited: false,
            in_folder: false,
            in_folders: [],
            validation: null
        },
        {
            doc_id: "FREAECB75FB2575899E6B4",
            reference_url: "/d/TGI/Paris/2007/FREAECB75FB2575899E6B4",
            titre:
                "Tribunal de grande instance de Paris, 3e chambre 1re section, 20 février 2007, n° 03&#x2F;04134, S.A. EDITIONS MANGO",
            raw_keywords: [
                "Musée",
                "Édition",
                "Sociétés",
                "Livre",
                "Oeuvre d'art",
                "Commercialisation",
                "Manque à gagner",
                "Monde",
                "Droits d'auteur",
                "Ouvrage"
            ],
            html_keywords: [
                "Musée",
                "Édition",
                "Sociétés",
                "Livre",
                "Oeuvre d&#x27;art",
                "Commercialisation",
                "Manque à gagner",
                "Monde",
                "Droits d&#x27;auteur",
                "Ouvrage"
            ],
            solution: null,
            arret_dec_att: null,
            sommaire_ana:
                "T R I B U N A L D E GRANDE I N S T A N C E D E P A R I S ■ 3e chambre 1re section N° RG : 03/04134 N° MINUTE : Assignation du : 12 Novembre 2002 JUGEMENT rendu le 20 Février 2007 DEMANDERESSES Madame D E G 23 rue des Trois-Frères [] Madame Z A [] [] représentées par Me Jean-Marc FELZENSZWALBE, avocat au barreau de PARIS, vestiaire C.119 DÉFENDERESSES S.A. EDITIONS MANGO 2-4 rue Z [] représentée par Me Emmanuel PIERRAT – Cabinet PIERRAT avocat au barreau de PARIS, vestiaire L.166 La REUNION DES MUSEES NATIONAUX - RMN …",
            juridiction: "TGI",
            automatic_metadata: {},
            number_of_comments: 0,
            reference_citation:
                "TGI Paris, 3e ch. 1re sect., 20 févr. 2007, n° 03/04134",
            visited: false,
            in_folder: false,
            in_folders: [],
            validation: null
        }
    ]
};
var access_premium = true
